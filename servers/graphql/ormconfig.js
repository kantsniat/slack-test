/**
 * @type {import("typeorm").ConnectionOptions}
 */
module.exports = {
  name: 'default',
  type: 'postgres',
  host: process.env.PG_HOST,
  port: +process.env.PG_PORT,
  username: process.env.PG_USER,
  password: process.env.PG_PASSWORD,
  database: process.env.PG_DB,
  synchronize: false, // Warn: dont set this to true
  migrations: ['src/migration/**/*.ts'],
  entities: ['src/entity/**/*.ts'],
  cli: {
    entitiesDir: 'src/entity',
    migrationsDir: 'src/migration',
  },
  logging: ['error', 'warn'],
};
