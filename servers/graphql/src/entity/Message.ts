import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import BaseEntity from '../repository/BaseEntity';
import { Channel } from './Channel';
import { User } from './User';

@Entity({
  name: 'message',
})
export class Message extends BaseEntity {
  @Column({
    type: 'text',
  })
  text: string;

  @ManyToOne(() => User, user => user.messages)
  @JoinColumn({
    name: 'user_id',
    referencedColumnName: 'id',
  })
  user?: User;

  @ManyToOne(() => User, user => user.messages)
  @JoinColumn({
    name: 'author_id',
    referencedColumnName: 'id',
  })
  author: User;

  @ManyToOne(() => User, user => user.messages)
  @JoinColumn({
    name: 'channel_id',
    referencedColumnName: 'id',
  })
  channel?: Channel;

  @Column({
    name: 'user_id',
    type: 'integer',
    nullable: true,
  })
  userId?: number;

  @Column({
    name: 'author_id',
    type: 'integer',
  })
  authorId?: number;

  @Column({
    name: 'channel_id',
    type: 'integer',
    nullable: true,
  })
  channelId?: number;
}
