import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import BaseEntity from '../repository/BaseEntity';
import { Channel } from './Channel';
import { User } from './User';

@Entity({
  name: 'user_channel',
})
export class UserChannel extends BaseEntity {
  @Column({
    name: 'user_id',
    type: 'integer',
  })
  userId: number;

  @Column({
    name: 'channel_id',
    type: 'integer',
  })
  channelId: number;

  @ManyToOne(() => User)
  @JoinColumn({
    name: 'user_id',
    referencedColumnName: 'id',
  })
  user: User;

  @ManyToOne(() => Channel)
  @JoinColumn({
    name: 'user_id',
    referencedColumnName: 'id',
  })
  channel: Channel;
}
