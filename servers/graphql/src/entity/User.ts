import { Column, Entity, OneToMany } from 'typeorm';
import { TableName } from '../constant/TableName';
import BaseEntity from '../repository/BaseEntity';
import { Message } from './Message';
import { UserChannel } from './UserChannel';

@Entity({
  name: TableName.user,
})
export class User extends BaseEntity {
  @Column({
    type: 'varchar',
  })
  firstname: string;

  @Column({
    type: 'varchar',
  })
  lastname: string;

  @Column({
    length: 60,
    type: 'varchar',
    unique: true,
  })
  username: string;

  @Column({
    length: 60,
    nullable: true,
    type: 'varchar',
  })
  password: string;

  @Column({
    type: 'varchar',
    length: 60,
  })
  phone: string;

  @OneToMany(() => Message, message => message.user)
  messages: Message[];

  @OneToMany(() => UserChannel, userChannel => userChannel.user)
  userChannels: UserChannel[];
}
