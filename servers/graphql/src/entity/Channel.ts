import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import BaseEntity from '../repository/BaseEntity';
import { Message } from './Message';
import { User } from './User';
import { UserChannel } from './UserChannel';

@Entity({
  name: 'channel',
})
export class Channel extends BaseEntity {
  @Column({
    length: 100,
    type: 'varchar',
  })
  name: string;

  @Column({
    name: 'is_private',
    type: 'boolean',
  })
  isPrivate: boolean;

  @ManyToOne(() => User)
  @JoinColumn({
    name: 'author_id',
    referencedColumnName: 'id',
  })
  author: User;

  @Column({
    name: 'author_id',
    type: 'integer',
  })
  authorId: number;

  @OneToMany(() => Message, message => message.channel)
  messages: Message[];

  @OneToMany(() => UserChannel, userChannel => userChannel.channel)
  userChannels: UserChannel[];
}
