import { makeExecutableSchema } from '@graphql-tools/schema';
import { ApolloServer } from 'apollo-server';
import { readFileSync } from 'fs';
import { applyMiddleware } from 'graphql-middleware';
import path from 'path';
import { getCustomRepository } from 'typeorm';
import { permissions } from './apollo/permissions';
import { resolvers } from './apollo/resolvers';
import { getConnection } from './repository/connection/createConnection';
import createRepositories from './repository/createRepositories';
import { getDataLoaders } from './repository/dataloader/index';
import { pubsub, redis } from './repository/redis/redis';
import { UserRepository } from './repository/UserRepository';
import jaegerPlugin from './tracing/jaegerPlugin';
import Context from './types/Context';

const typeDefs = readFileSync(path.join(__dirname, './apollo/schema.graphql'), 'utf-8');

const schema = applyMiddleware(
  makeExecutableSchema({
    resolvers,
    typeDefs,
  }),
  permissions
);
const apolloServer = new ApolloServer({
  schema,
  context: async (apolloContext): Promise<Context> => {
    const repository = createRepositories();
    const context: Omit<Context, 'dataloader' | 'currentUser'> = {
      ...repository,
      jaegerLogger: {
        // eslint-disable-next-line no-console
        error: error => console.log('jaegerError', error),
        // eslint-disable-next-line no-console
        log: log => console.log('jaegerLog', log),
      },
      redis,
    };
    const dataloader = getDataLoaders(context);
    const currentUser = await context.user.getUserFromRequest(
      apolloContext,
      context.redis,
      dataloader
    );

    return {
      ...context,
      dataloader,
      currentUser: currentUser as any,
    };
  },
  subscriptions: {
    onConnect: async connectionParams => {
      const { authToken } = connectionParams as any;
      if (!authToken) {
        return {};
      }
      const userId = await redis.get(authToken);
      if (!userId) {
        return {};
      }
      const currentUser = await getCustomRepository(UserRepository).findOne(userId);
      if (currentUser) {
        await redis.set(`user:${currentUser.id}:lastSeen`, 0);
        pubsub.publish('userLoggedIn', { userLoggedIn: currentUser });
        return { currentUser };
      }
      return {};
    },
    onDisconnect: async (_connectionParams, context) => {
      const c: Context = await context.initPromise;
      const { currentUser } = c;

      if (currentUser) {
        await redis.set(`user:${currentUser.id}:lastSeen`, new Date().getTime());
        pubsub.publish('userLoggedOut', { userLoggedOut: currentUser });
        return { currentUser };
      }
      return {};
    },
    path: '/',
  },
  plugins: [jaegerPlugin],
});

getConnection().then(() => {
  apolloServer.listen().then(({ url, subscriptionsUrl }) => {
    // eslint-disable-next-line no-console
    console.log(`🚀 Server ready at ${url}`);
    // eslint-disable-next-line no-console
    console.log(`🚀 Subscription ready at ${subscriptionsUrl}`);
  });
});
