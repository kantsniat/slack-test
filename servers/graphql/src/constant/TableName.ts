/* eslint-disable no-unused-vars */
export enum TableName {
  user = 'user',
  message = 'message',
  channel = 'channel',
  userChannel = 'user_channel',
}
