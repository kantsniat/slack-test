import DataLoader from 'dataloader';
import { Channel } from '../entity/Channel';
import { Message } from '../entity/Message';
import { User } from '../entity/User';
import { UserChannel } from '../entity/UserChannel';

export default interface DataloaderInterface {
  user: DataLoader<number, User, number>;
  channel: DataLoader<number, Channel, number>;
  userChannel: DataLoader<number, UserChannel, number>;
  message: DataLoader<number, Message, number>;
}
