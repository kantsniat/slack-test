import { Rule, RuleAnd, RuleOr } from 'graphql-shield/dist/rules';
import { MutationResolvers, QueryResolvers } from '../generated/graphql';

interface PermissionShieldType {
  Mutation: Partial<Record<'*' | keyof MutationResolvers, Rule | RuleAnd | RuleOr>>;
  Query: Partial<Record<'*' | keyof QueryResolvers, Rule | RuleAnd | RuleOr>>;
}

type PermissionShieldInterface = Partial<PermissionShieldType>;

export default PermissionShieldInterface;
