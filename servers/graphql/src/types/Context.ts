import { Redis } from 'ioredis';
import { Span } from 'opentracing';
import { User } from '../entity/User';
import { ChannelRepository } from '../repository/ChannelRepository';
import { MessageRepository } from '../repository/MessageRepository';
import { UserChannelRepository } from '../repository/UserChannelRepository';
import { UserRepository } from '../repository/UserRepository';
import DataloaderInterface from './DataloaderInterface';

export interface JaegerLoggerInterface {
  // eslint-disable-next-line no-unused-vars
  error: (error: Error) => void;
  // eslint-disable-next-line no-unused-vars
  log: (log: any) => void;
}

export default interface Context {
  user: UserRepository;
  channel: ChannelRepository;
  message: MessageRepository;
  userChannel: UserChannelRepository;
  dataloader: DataloaderInterface;
  redis: Redis;
  currentUser: User;
  jaegerLogger: JaegerLoggerInterface;
  rootSpan?: Span;
}
