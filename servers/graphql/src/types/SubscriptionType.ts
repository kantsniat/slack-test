import { Subscription } from '../generated/graphql';

// eslint-disable-next-line no-unused-vars
type subscriptionType = { [K in keyof Subscription]?: any };

export default subscriptionType;
