/* eslint-disable class-methods-use-this */
import { MigrationInterface, QueryRunner, Table } from 'typeorm';
import { TableName } from '../constant/TableName';
import { baseTableColumnsFields } from '../utils/migrationUtils';

export class CreateMessageTable1633836517047 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: TableName.message,
        columns: [
          ...baseTableColumnsFields,
          {
            name: 'text',
            type: 'text',
          },
          {
            name: 'author_id',
            type: 'integer',
          },
          {
            name: 'user_id',
            type: 'integer',
            isNullable: true,
          },
          {
            name: 'channel_id',
            type: 'integer',
            isNullable: true,
          },
        ],
        foreignKeys: [
          {
            name: 'message-user',
            referencedTableName: TableName.user,
            columnNames: ['user_id'],
            referencedColumnNames: ['id'],
          },
          {
            name: 'message-user-author',
            referencedTableName: TableName.user,
            columnNames: ['author_id'],
            referencedColumnNames: ['id'],
          },
          {
            name: 'message-channel',
            referencedTableName: TableName.channel,
            columnNames: ['channel_id'],
            referencedColumnNames: ['id'],
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('channel');
  }
}
