/* eslint-disable class-methods-use-this */
import { hashSync } from 'bcrypt';
import { MigrationInterface, QueryRunner } from 'typeorm';
import { User } from '../entity/User';

const users: Partial<User>[] = [
  {
    lastname: 'John',
    firstname: 'Doe',
    username: 'john123',
    password: hashSync('password1', 10),
    phone: '+261449772724',
  },
  {
    lastname: 'Allegra',
    firstname: 'Yosuke',
    username: 'allegra',
    password: hashSync('password2', 10),
    phone: '+261349772721',
  },
  {
    lastname: 'Guillaume',
    firstname: 'Dubois',
    username: 'gui123',
    password: hashSync('password1', 10),
    phone: '+261349372724',
  },
  {
    lastname: 'Camille',
    firstname: 'Martin',
    username: 'camille123',
    password: hashSync('password4', 10),
    phone: '+261349772724',
  },
  {
    lastname: 'Charles',
    firstname: 'Martin',
    username: 'charles',
    password: hashSync('password4', 10),
    phone: '+261349772724',
  },
];

export class UserSeed1633879528154 implements MigrationInterface {
  public async up(queryBuilder: QueryRunner): Promise<void> {
    let params: any[] = [];
    for (const user of users) {
      params = params.concat([
        new Date().toISOString(),
        user.firstname,
        user.lastname,
        user.username,
        user.password,
        user.phone,
      ]);
    }

    const values: string[] = [];

    for (const user of users) {
      values.push(
        `(${[
          `'${new Date().toISOString()}'`,
          'DEFAULT',
          'DEFAULT',
          `'${user.username}'`,
          `'${user.password}'`,
          `'${user.firstname}'`,
          `'${user.lastname}'`,
          `'${user.phone}'`,
        ].join(',')})`
      );
    }

    await queryBuilder.query(
      `
        INSERT INTO "user" 
            (created_at, updated_at, removed, username, password, firstname, lastname, phone)
        VALUES ${values.join(',')};
        `
    );
  }

  public async down(queryBuilder: QueryRunner): Promise<void> {
    await queryBuilder.query('TRUNCATE TABLE "user"');
  }
}
