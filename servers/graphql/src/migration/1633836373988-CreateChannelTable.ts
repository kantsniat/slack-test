/* eslint-disable class-methods-use-this */
import { MigrationInterface, QueryRunner, Table } from 'typeorm';
import { baseTableColumnsFields } from '../utils/migrationUtils';

export class CreateChannelTable1633836373988 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'channel',
        columns: [
          ...baseTableColumnsFields,
          {
            name: 'name',
            type: 'varchar',
          },
          {
            name: 'is_private',
            type: 'boolean',
          },
          {
            name: 'author_id',
            type: 'integer',
          },
        ],
        foreignKeys: [
          {
            name: 'channel-author',
            referencedTableName: 'user',
            columnNames: ['author_id'],
            referencedColumnNames: ['id'],
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('channel');
  }
}
