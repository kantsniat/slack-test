/* eslint-disable class-methods-use-this */
import { MigrationInterface, QueryRunner, Table } from 'typeorm';
import { TableName } from '../constant/TableName';
import { baseTableColumnsFields } from '../utils/migrationUtils';

export class CreateUserChannelTable1633836782758 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: TableName.userChannel,
        columns: [
          ...baseTableColumnsFields,
          {
            name: 'user_id',
            type: 'integer',
          },
          {
            name: 'channel_id',
            type: 'integer',
          },
        ],
        foreignKeys: [
          {
            name: 'user_channel_user',
            referencedTableName: TableName.user,
            columnNames: ['user_id'],
            referencedColumnNames: ['id'],
          },
          {
            name: 'user_channel_channel',
            referencedTableName: TableName.channel,
            columnNames: ['channel_id'],
            referencedColumnNames: ['id'],
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('channel');
  }
}
