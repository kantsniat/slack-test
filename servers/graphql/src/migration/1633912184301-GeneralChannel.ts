/* eslint-disable class-methods-use-this */
import { MigrationInterface, QueryRunner } from 'typeorm';

export class GeneralChannel1633912184301 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        INSERT INTO public.channel(
	created_at, removed, name,is_private,  author_id)
	VALUES ('${new Date().toISOString()}', FALSE, 'General', FALSE, 1);
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('TRUNCATE TABLE channel');
  }
}
