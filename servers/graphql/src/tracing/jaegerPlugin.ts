/* eslint-disable no-console */
/* tslint:disable no-console */
import OpentracingPlugin from 'apollo-opentracing';
import Context from '../types/Context';
import { jaegerErrorLogFn, jaegerInfosLogFn } from './jaegerUtils';
import { jaegerTracer } from './tracing';

const jaegerPlugin = OpentracingPlugin({
  server: jaegerTracer,
  local: jaegerTracer,
  onFieldResolveFinish(error, result, span) {
    if (error) {
      jaegerErrorLogFn(span, error);
    } else if (result instanceof Error) {
      jaegerErrorLogFn(span, result);
      span.log({
        event: 'error',
        message: result.message,
        stack: result.stack,
      });
    } else if (typeof result !== 'object') {
      span.setTag('value', result);
    }
  },
  shouldTraceRequest: info => info.request.operationName !== 'IntrospectionQuery',
  onRequestResolve(span, info) {
    span.setOperationName(info.request.operationName || 'request');
    const appContext: Context = info.context as any;
    span.setTag('variables', info.request.variables);
    span.setTag('userId', appContext.currentUser?.id);
    appContext.jaegerLogger = {
      error: (error: Error) => jaegerErrorLogFn(span, error),
      log: (log: any) => jaegerInfosLogFn(span, log),
    };
    appContext.rootSpan = span;
  },
});

export default jaegerPlugin;
