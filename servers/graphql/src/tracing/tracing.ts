import { initTracerFromEnv, TracingConfig } from 'jaeger-client';

export const initTracer = (serviceName: string) => {
  const tracingConfig: TracingConfig = {
    serviceName,
    sampler: {
      type: 'const',
      param: 1,
    },
    reporter: {
      logSpans: false,
      // Using HTTP transport over UDP
      // Reason: UDP max packet size limited for some spans
      collectorEndpoint: 'http://jaeger:14268/api/traces',
    },
  };
  const options = {
    logger: {
      info: function logInfo(msg: string) {
        // eslint-disable-next-line no-console
        console.log('INFO  ', msg);
      },
      error: function logError(msg: string) {
        // eslint-disable-next-line no-console
        console.log('ERROR ', msg);
      },
    },
  };
  return initTracerFromEnv(tracingConfig, options);
};

export const jaegerTracer = initTracer('graphql-api');
