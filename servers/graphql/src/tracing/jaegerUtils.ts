import { JaegerTracer } from 'jaeger-client';
import { Span, Tags } from 'opentracing';
import { initTracer, jaegerTracer } from './tracing';

/**
 * Get span merged with graphql-service
 */
export const jaegerCreateChildSpanFn = (
  spanName: string,
  rootSpan: Span | undefined
): Span | undefined => {
  if (!rootSpan) {
    return;
  }
  return jaegerTracer.startSpan(spanName, {
    childOf: rootSpan,
  });
};

export const jaegerErrorLogFn = (span: Span | undefined, error: Error) => {
  if (span) {
    span.setTag(Tags.ERROR, 'true');
    span.log({
      ...error,
      message: error.message,
      stack: error.stack,
    });
  }
};

/**
 * Finish span
 */
export const jaegerFinishSpanFn = (span: Span | undefined) => {
  if (span) {
    span.finish();
  }
};

/**
 * Finish span
 */
export const jaegerSetTagFn = (span: Span | undefined, tagKey: string, value: any) => {
  if (span) {
    span.setTag(tagKey, value);
  }
};

/**
 * Log infos
 */
export const jaegerInfosLogFn = (span: Span | undefined, log: any) => {
  if (span) {
    span.log({
      log,
    });
  }
};

export const jaegerPerformanceWarnLogFn = (span: Span | undefined, message: any) => {
  if (span) {
    span.setTag('performanceIssue', true);
    span.log({ performanceMessage: message });
  }
};

/**
 * Create root span by tracer
 */
export const jaegerCreateRootSpanFn = (tracer: JaegerTracer, spanName: string) => {
  return tracer.startSpan(spanName);
};

/**
 * create tracer
 */
export const createJaegerTracerFn = (tracerName = 'graphql-api') => {
  return initTracer(tracerName);
};

/**
 * Log root error in jaeger
 */
export const jaegerHandleRootError = (tracer: JaegerTracer, spanName: string, error: Error) => {
  const errorSpan = jaegerCreateRootSpanFn(jaegerTracer, 'backgroundTaskConsumerError');
  jaegerErrorLogFn(errorSpan, error);
  jaegerFinishSpanFn(errorSpan);
};
