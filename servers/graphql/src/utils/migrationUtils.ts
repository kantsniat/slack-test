import { TableColumnOptions } from 'typeorm/schema-builder/options/TableColumnOptions';

export const baseTableColumnsFields: TableColumnOptions[] = [
  {
    name: 'id',
    type: 'integer',
    isPrimary: true,
    isGenerated: true,
    generationStrategy: 'increment',
  },
  {
    name: 'created_at',
    type: 'timestamp',
  },
  {
    isNullable: true,
    name: 'updated_at',
    type: 'timestamp',
  },
  {
    name: 'removed',
    type: 'boolean',
    default: false,
  },
];
