/* eslint-disable no-unused-vars */
import { withFilter } from 'apollo-server';
import { Subscription } from '../generated/graphql';
import { pubsub } from '../repository/redis/redis';
import Context from '../types/Context';

export const subscribeWithFilter = <
  T extends keyof Subscription = 'userInfoUpdated',
  Args = any,
  PayloadType = Record<string, never>
>(
  subscriptionName: T,
  filterFunction: (
    payload: PayloadType & Record<T, Subscription[T]>,
    args: Args,
    context: Context
  ) => boolean
) => {
  return {
    subscribe: withFilter(() => pubsub.asyncIterator([subscriptionName]), filterFunction),
  };
};
