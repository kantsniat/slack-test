import { QueryResolvers } from '../../../generated/graphql';
import Context from '../../../types/Context';

export const channelQuery: {
  channel: QueryResolvers<Context>['channel'];
} = {
  channel: (_, { channelId }, ctx) => ctx.dataloader.channel.load(+channelId) as any,
};
