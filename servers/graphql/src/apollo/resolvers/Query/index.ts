import { QueryResolvers } from '../../../generated/graphql';
import Context from '../../../types/Context';
import { channelQuery } from './channel';
import { userQuery } from './user';

export const Query: QueryResolvers<Context> = {
  ...userQuery,
  ...channelQuery,
};
