import { QueryResolvers } from '../../../generated/graphql';
import Context from '../../../types/Context';

export const userQuery: {
  me: QueryResolvers<Context>['me'];
} = {
  me: (_, __, ctx) => ctx.currentUser as any,
};
