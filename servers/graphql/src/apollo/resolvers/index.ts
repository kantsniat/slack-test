/* eslint-disable no-underscore-dangle */
import { DateTimeResolver } from 'graphql-scalars';
import { Resolvers } from '../../generated/graphql';
import Context from '../../types/Context';
import Channel from './Channel';
import Message from './Message';
import { Mutation } from './Mutation';
import { Query } from './Query';
import { Subscription } from './Subscription';
import User from './User';

export const resolvers: Resolvers<Context> = {
  Query,
  Mutation,
  User,
  Channel,
  Message,
  Subscription,
  Date: DateTimeResolver,
  Destination: {
    __resolveType(parent) {
      if ((parent as any).name) {
        return 'Channel';
      }
      return 'User';
    },
  },
};
