import subscriptionType from '../../../types/SubscriptionType';
import { channelSubscription } from './channel';
import { messageSubscription } from './message';
import { userSubscription } from './user';

export const Subscription: subscriptionType = {
  ...userSubscription,
  ...messageSubscription,
  ...channelSubscription,
};
