import subscriptionType from '../../../types/SubscriptionType';
import { subscribeWithFilter } from '../../../utils/subscriptionUtils';

export const userSubscription: subscriptionType = {
  userInfoUpdated: subscribeWithFilter('userInfoUpdated', (_payload, _, context) => {
    return Boolean(context.currentUser);
  }),
  userLoggedIn: subscribeWithFilter('userLoggedIn', (_payload, _, context) => {
    return Boolean(context.currentUser);
  }),
  userLoggedOut: subscribeWithFilter('userLoggedOut', (_payload, _, context) => {
    return Boolean(context.currentUser);
  }),
};
