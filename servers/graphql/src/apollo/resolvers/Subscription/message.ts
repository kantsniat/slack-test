import subscriptionType from '../../../types/SubscriptionType';
import { subscribeWithFilter } from '../../../utils/subscriptionUtils';

export const messageSubscription: subscriptionType = {
  messageAdded: subscribeWithFilter<'messageAdded', any, { userIds: number[]; isPublic: boolean }>(
    'messageAdded',
    (payload, _, context) => {
      if (context.currentUser) {
        const { isPublic, userIds } = payload;
        if (isPublic) {
          return true;
        }
        return userIds.includes(context.currentUser.id);
      }
      return false;
    }
  ),
};
