import subscriptionType from '../../../types/SubscriptionType';
import { subscribeWithFilter } from '../../../utils/subscriptionUtils';

export const channelSubscription: subscriptionType = {
  channelAdded: subscribeWithFilter('channelAdded', (payload, _, context) => {
    if (payload.channelAdded.isPrivate) {
      return Boolean(
        context.currentUser && context.currentUser.id === payload.channelAdded.authorId
      );
    }
    return true;
  }),
  userChannelMemberUpdated: subscribeWithFilter<
    'userChannelMemberUpdated',
    any,
    { userIds: number[] }
  >('userChannelMemberUpdated', (payload, _, context) => {
    return context.currentUser && payload.userIds.includes(+context.currentUser.id);
  }),
};
