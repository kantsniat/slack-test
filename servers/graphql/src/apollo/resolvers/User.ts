import { Resolvers } from '../../generated/graphql';
import Context from '../../types/Context';

const User: Resolvers<Context>['User'] = {
  channels: (_, __, ctx) => ctx.channel.findUserChannels(ctx.currentUser.id) as any,
  messages: async (_, { filter }, ctx) => {
    const { userId, limit, offset } = filter;
    const user = await ctx.dataloader.user.load(+userId);
    if (!user) {
      throw new Error('USER_NOT_FOUND');
    }
    const ourMessages = await ctx.message.findUserMessageAndCount({
      meId: ctx.currentUser.id,
      userId,
      limit: limit || undefined,
      offset: offset || undefined,
    });

    return {
      count: ourMessages.count,
      messages: ourMessages.messages as any,
      userId,
      user: user as any,
    };
  },
  lastConnection: async (parent, _, ctx) => {
    const lastConnection = await ctx.redis.get(`user:${parent.id}:lastSeen`);
    return lastConnection === null ? null : +lastConnection;
  },
  users: async (_, __, ctx) => {
    return ctx.user.find() as any;
  },
  isMe: (parent, __, ctx) => +parent.id === ctx.currentUser.id,
};

export default User;
