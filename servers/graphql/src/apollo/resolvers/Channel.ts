import { Resolvers } from '../../generated/graphql';
import Context from '../../types/Context';

const Channel: Resolvers<Context>['Channel'] = {
  author: (parent, _, ctx) => ctx.dataloader.user.load(parent.authorId) as any,
  messages: async (parent, { filter }, ctx) => {
    const { limit, offset } = filter;
    const messagesData = await ctx.message.findChannelMessageAndCount({
      channelId: parent.id,
      limit: limit || undefined,
      offset: offset || undefined,
    });
    return {
      channelId: parent.id,
      channel: parent,
      count: messagesData.count,
      messages: messagesData.messages as any,
    };
  },
  members: async (parent, _, ctx) => ctx.user.findChannelMembers(parent.id) as any,
  isAuthor: (parent, _, ctx) => +ctx.currentUser.id === +parent.authorId,
};

export default Channel;
