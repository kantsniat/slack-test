import { Resolvers } from '../../generated/graphql';
import Context from '../../types/Context';

const Message: Resolvers<Context>['Message'] = {
  author: (parent, _, ctx) => ctx.dataloader.user.load(parent.authorId) as any,
  isMine: (parent, _, ctx) => parent.authorId === ctx.currentUser.id,
};

export default Message;
