import { MutationResolvers } from '../../../generated/graphql';
import Context from '../../../types/Context';

export const userMutation: {
  updateUser: MutationResolvers<Context>['updateUser'];
} = {
  updateUser: async (_, { input }, ctx) => {
    const { firstname, lastname, phone } = input;
    return ctx.user.updateUser({ firstname, lastname, userId: ctx.currentUser.id, phone }) as any;
  },
};
