import { MutationResolvers } from '../../../generated/graphql';
import Context from '../../../types/Context';

export const messageMutation: {
  addDiscussionMessage: MutationResolvers<Context>['addDiscussionMessage'];
  addChannelMessage: MutationResolvers<Context>['addChannelMessage'];
} = {
  addDiscussionMessage: async (_, { input }, ctx) => {
    const { text, userId } = input;
    return ctx.message.addDiscussionMessage(userId, ctx.currentUser.id, text) as any;
  },
  addChannelMessage: async (_, { input }, ctx) => {
    const { text, channelId } = input;
    const channel = await ctx.dataloader.channel.load(+channelId);
    if (!channel) {
      throw new Error('CHANNEL_NOT_FOUND');
    }
    return ctx.message.addChannelMessage(
      ctx.currentUser.id,
      channelId,
      text,
      !channel.isPrivate,
      ctx.user
    ) as any;
  },
};
