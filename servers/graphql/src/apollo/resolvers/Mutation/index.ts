import { MutationResolvers } from '../../../generated/graphql';
import Context from '../../../types/Context';
import { authenticationMutation } from './authentication';
import { channelMutation } from './channel';
import { messageMutation } from './message';
import { userMutation } from './user';

export const Mutation: MutationResolvers<Context> = {
  ...authenticationMutation,
  ...channelMutation,
  ...userMutation,
  ...messageMutation,
};
