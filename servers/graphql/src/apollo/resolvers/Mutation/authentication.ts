import { MutationResolvers } from '../../../generated/graphql';
import Context from '../../../types/Context';

export const authenticationMutation: {
  login: MutationResolvers<Context>['login'];
} = {
  login: async (_, { input }, ctx) => {
    const { password, username } = input;
    return ctx.user.authenticate(username, password, ctx.redis);
  },
};
