import { MutationResolvers } from '../../../generated/graphql';
import Context from '../../../types/Context';

export const channelMutation: {
  addChannel: MutationResolvers<Context>['addChannel'];
  removeChannel: MutationResolvers<Context>['removeChannel'];
  addChannelParticipants: MutationResolvers<Context>['addChannelParticipants'];
  removeChannelParticipants: MutationResolvers<Context>['removeChannelParticipants'];
} = {
  addChannel: async (_, { input }, ctx) => {
    const { isPublic, name } = input;
    return ctx.channel.addChannel({ isPublic, authorId: ctx.currentUser.id, name }, ctx) as any;
  },
  removeChannel: async (_, { channelId }, ctx) => {
    return ctx.channel.removeChannel(channelId) as any;
  },
  addChannelParticipants: async (_, { input }, ctx) => {
    const { channelId, userId } = input;
    return ctx.userChannel.addUserInChannel(channelId, userId, ctx) as any;
  },
  removeChannelParticipants: async (_, { input }, ctx) => {
    const { channelId, userId } = input;
    return ctx.userChannel.removeInChannel(channelId, userId, ctx) as any;
  },
};
