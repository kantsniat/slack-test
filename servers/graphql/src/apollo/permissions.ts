import { AuthenticationError } from 'apollo-server';
import { rule, shield } from 'graphql-shield';
import ErrorMessage from '../constant/ErrorMessage';
import Context from '../types/Context';
import PermissionShieldInterface from '../types/PermissionShieldInterface';

const isUserConnected = rule()(async (_, __, context: Context) => {
  if (!context.currentUser) {
    return new AuthenticationError(ErrorMessage.NOT_AUTHENTICATED);
  }
  return true;
});

const canGetChannel = rule()(async (_, { channelId }, context: Context) => {
  const channel = await context.dataloader.channel.load(+channelId);
  if (!channel) {
    return new Error('CHANNEL_NOT_FOUND');
  }
  if (!channel.isPrivate) {
    return true;
  }
  const userChannel = await context.userChannel.count({
    channelId,
    userId: context.currentUser.id,
  });
  return Boolean(userChannel);
});

const isAllPermission = rule()(() => true);

const permissionShield: PermissionShieldInterface = {
  Query: {
    '*': isUserConnected,
  },
  Mutation: {
    '*': isUserConnected,
    login: isAllPermission,
  },
};

export const permissions = shield(permissionShield, {
  allowExternalErrors: true,
});
