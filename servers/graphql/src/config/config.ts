const config = {
  redis: {
    host: process.env.REDIS_HOST || 'redis',
    password: process.env.REDIS_PASSWORD,
    port: +`${process.env.REDIS_PORT || 6379}`,
    useTls: Boolean(process.env.REDIS_TLS),
  },
  port: process.env.PORT || 4000,
};

export default config;
