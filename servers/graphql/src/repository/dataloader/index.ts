import Context from '../../types/Context';
import DataloaderInterface from '../../types/DataloaderInterface';
import { loadDataloaderWithKeysId } from './dataLoaderUtils';

export const getDataLoaders = (
  ctx: Omit<Context, 'dataloader' | 'currentUser'>
): DataloaderInterface => {
  return {
    user: loadDataloaderWithKeysId(ctx.user),
    channel: loadDataloaderWithKeysId(ctx.channel),
    message: loadDataloaderWithKeysId(ctx.message),
    userChannel: loadDataloaderWithKeysId(ctx.userChannel),
  };
};
