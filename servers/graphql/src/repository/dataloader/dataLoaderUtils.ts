import DataLoader from 'dataloader';
import { In, Repository } from 'typeorm';

/**
 * Clear all data in loader
 */
export const clearAllLoaderFn = (dataloader: DataLoader<any, any, any>) => {
  dataloader.clearAll();
};

/**
 * Load repository with number id
 * @param repository
 */
export const loadDataloaderWithKeysId = <T extends { id: number }>(repository: Repository<T>) => {
  const dataloader = new DataLoader<number, T | null, number>(
    async keysArg => {
      const keysStr: number[] = keysArg as any;
      const keys = keysStr.filter(key => !isNaN(key));
      if (!keys.length) {
        return keysStr.map(() => null);
      }
      clearAllLoaderFn(dataloader);
      const entities = await repository.find({
        where: {
          id: In(keys),
        },
        cache: true,
      });

      const roleMap: Record<number, T> = {};
      entities.forEach(entity => {
        roleMap[+entity.id] = entity;
      });

      return keys.map(key => roleMap[+key]);
    },
    {
      cacheKeyFn: key => +key,
    }
  );
  return dataloader;
};
