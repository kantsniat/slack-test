import { BeforeInsert, BeforeUpdate, Column, PrimaryGeneratedColumn } from 'typeorm';

export default class BaseEntity {
  @PrimaryGeneratedColumn({
    type: 'integer',
  })
  id: number;

  @Column({
    name: 'created_at',
    type: 'timestamp',
  })
  createdAt: Date;

  @Column({
    type: 'integer',
    default: 0,
  })
  removed: number;

  @Column({
    name: 'updated_at',
    nullable: true,
    type: 'timestamp',
  })
  updatedAt?: Date;

  @BeforeUpdate()
  beforeUpdate?: () => void = () => {
    this.updatedAt = new Date();
  };

  @BeforeInsert()
  beforeInsert?: () => void = () => {
    this.createdAt = new Date();
  };
}
