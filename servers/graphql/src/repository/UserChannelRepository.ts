import { EntityRepository, Repository } from 'typeorm';
import ErrorMessage from '../constant/ErrorMessage';
import { UserChannel } from '../entity/UserChannel';
import Context from '../types/Context';
import { pubsub } from './redis/redis';

@EntityRepository(UserChannel)
export class UserChannelRepository extends Repository<UserChannel> {
  async removeUserChannel(channelId: number) {
    const channel = await this.findOne(channelId);
    if (!channel) {
      throw new Error('CHANNEL_NOT_FOUND');
    }
    if (!channel.removed) {
      channel.removed = 1;
      return this.save(channel);
    }
    return channel;
  }

  async addUserInChannel(channelId: number, userId: number, ctx: Context) {
    const channel = await ctx.dataloader.channel.load(channelId);
    if (!channel) {
      throw new Error('CHANNEL_NOT_FOUND');
    }

    if (+channel.authorId !== +ctx.currentUser.id) {
      throw new Error('NOT_AUTHORIZED');
    }

    const user = await ctx.dataloader.user.load(userId);

    if (!user) {
      throw new Error(ErrorMessage.USER_NOT_FOUND);
    }

    const userChannel = await this.findOne({
      channelId,
      userId,
    });

    if (userChannel) {
      throw new Error('USER_ALREADY_MEMBER');
    }

    const newUserChannel = this.create({ channelId, userId });
    await this.save(newUserChannel);

    const channelMembers = await ctx.user.findChannelMembers(channelId);
    const userIds = channelMembers.map(({ id }) => +id);
    userIds.push(+userId);
    pubsub.publish('userChannelMemberUpdated', {
      userChannelMemberUpdated: channel,
      userIds,
    });
    return channel;
  }

  async removeInChannel(channelId: number, userId: number, ctx: Context) {
    const channel = await ctx.dataloader.channel.load(channelId);
    if (!channel) {
      throw new Error('CHANNEL_NOT_FOUND');
    }

    if (+channel.authorId !== +ctx.currentUser.id) {
      throw new Error('NOT_AUTHORIZED');
    }

    const user = await ctx.dataloader.user.load(userId);

    if (!user) {
      throw new Error(ErrorMessage.USER_NOT_FOUND);
    }

    const userChannel = await this.findOne({
      channelId,
      userId,
    });

    if (!userChannel) {
      throw new Error('USER_NOT_CHANNEL_MEMBER');
    }
    await this.remove(userChannel);
    const channelMembers = await ctx.user.findChannelMembers(channelId);
    const userIds = channelMembers.map(({ id }) => +id);
    userIds.push(+userId);
    pubsub.publish('userChannelMemberUpdated', {
      userChannelMemberUpdated: channel,
      userIds,
    });
    return channel;
  }
}
