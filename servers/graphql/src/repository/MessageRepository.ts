import { EntityRepository, Repository } from 'typeorm';
import { Message } from '../entity/Message';
import { pubsub } from './redis/redis';
import { UserRepository } from './UserRepository';

@EntityRepository(Message)
export class MessageRepository extends Repository<Message> {
  async addDiscussionMessage(userId: number, authorId: number, text: string) {
    const message = this.create({
      text,
      userId,
      authorId,
    });
    const savedMessage = await this.save(message);
    pubsub.publish('messageAdded', {
      messageAdded: savedMessage,
      userIds: [+authorId, +userId],
    });
    return savedMessage;
  }

  async addChannelMessage(
    authorId: number,
    channelId: number,
    text: string,
    isPublicChannel: boolean,
    userRepository: UserRepository
  ) {
    const message = this.create({
      text,
      channelId,
      authorId,
    });
    const savedMessage = await this.save(message);
    const userMember = isPublicChannel ? [] : await userRepository.findChannelMembers(channelId);
    const membersIds = userMember.map(({ id }) => +id);
    pubsub.publish('messageAdded', {
      messageAdded: savedMessage,
      isPublic: isPublicChannel,
      userIds: membersIds,
    });
    return savedMessage;
  }

  async removeMessage(messageId: number) {
    const message = await this.findOne(messageId);
    if (!message) {
      throw new Error('MESSAGE_NOT_FOUND');
    }
    if (!message.removed) {
      message.removed = 1;
      await this.save(message);
      return message;
    }
  }

  async findUserMessageAndCount(args: {
    meId: number;
    userId: number;
    limit?: number;
    offset?: number;
  }) {
    const { userId, limit, offset, meId } = args;
    const query = this.createQueryBuilder('m').where(
      '((m.user_id = :userId AND m.author_id = :meId) OR (m.user_id = :meId AND m.author_id = :userId))',
      {
        userId,
        meId,
      }
    );

    const count = await query.clone().getCount();

    if (limit && offset) {
      query.limit(limit).offset(offset);
    }

    query.orderBy('m.created_at', 'ASC');

    const messages = await query.getMany();

    return {
      count,
      messages,
    };
  }

  async findChannelMessageAndCount(args: { channelId: number; limit?: number; offset?: number }) {
    const { channelId, limit, offset } = args;
    const channelMessages = await this.findAndCount({
      where: {
        channelId,
      },
      order: {
        createdAt: 'DESC',
      },
      take: limit,
      skip: offset,
    });
    return {
      count: channelMessages[1],
      messages: channelMessages[0],
    };
  }
}
