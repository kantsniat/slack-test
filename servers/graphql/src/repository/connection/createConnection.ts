// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import ormConfig = require('../../../ormconfig');
import { createConnection } from 'typeorm';
import { Channel } from '../../entity/Channel';
import { Message } from '../../entity/Message';
import { User } from '../../entity/User';
import { UserChannel } from '../../entity/UserChannel';

export const getConnection = () =>
  createConnection({
    ...ormConfig,
    migrations: [],
    entities: [User, Message, Channel, UserChannel],
  });
