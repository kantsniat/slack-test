import { getCustomRepository } from 'typeorm';
import { ChannelRepository } from './ChannelRepository';
import { MessageRepository } from './MessageRepository';
import { UserChannelRepository } from './UserChannelRepository';
import { UserRepository } from './UserRepository';

const createRepositories = () => ({
  user: getCustomRepository(UserRepository),
  channel: getCustomRepository(ChannelRepository),
  message: getCustomRepository(MessageRepository),
  userChannel: getCustomRepository(UserChannelRepository),
});

export default createRepositories;
