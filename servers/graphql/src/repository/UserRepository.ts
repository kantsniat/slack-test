// eslint-disable-next-line import/no-extraneous-dependencies
import { ExpressContext } from 'apollo-server-express';
import { compareSync } from 'bcrypt';
import { randomBytes } from 'crypto';
import { Redis } from 'ioredis';
import { EntityRepository, Repository } from 'typeorm';
import ErrorMessage from '../constant/ErrorMessage';
import { TableName } from '../constant/TableName';
import { User } from '../entity/User';
import Context from '../types/Context';
import DataloaderInterface from '../types/DataloaderInterface';
import { pubsub } from './redis/redis';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  findByUsername(username: string): Promise<User | undefined> {
    return this.findOne({ username });
  }

  findChannelMembers(channelId: number) {
    return this.createQueryBuilder('u')
      .innerJoin(TableName.userChannel, 'uc', 'uc.user_id = u.id')
      .where('uc.channel_id = :channelId', { channelId })
      .getMany();
  }

  async authenticate(username: string, password: string, redis: Redis) {
    if (!username.trim()) {
      throw new Error(ErrorMessage.USER_NAME_IS_EMPTY);
    }
    if (!password.length) {
      throw new Error(ErrorMessage.PASSWORD_IS_EMPTY);
    }
    const user = await this.findByUsername(username);
    if (!user || !compareSync(password, user.password)) {
      throw new Error(ErrorMessage.LOGIN_OR_PASSWORD_INCORRECT);
    }
    const token = randomBytes(48).toString('base64');

    await redis.set(token, user.id);

    return token;
  }

  getUserFromRequest = async (
    expressContext: ExpressContext,
    redis: Redis,
    dataloader: DataloaderInterface
  ) => {
    const { req, connection } = expressContext;
    let userId: string | number | null = null;
    if (connection) {
      return (connection.context as Context).currentUser;
    }

    if (typeof req === 'string') {
      userId = +redis.get(req);
    } else if (!userId) {
      const headers = req?.headers;
      const authorization = (headers as any)?.authorization;
      if (authorization) {
        userId = await redis.get(authorization.replace('Bearer ', ''));
      }
    }
    if (userId) {
      return dataloader.user.load(+userId);
    }
    return null;
  };

  updateUser = async (args: {
    firstname: string;
    lastname: string;
    phone: string;
    userId: number;
  }) => {
    const { firstname, lastname, userId, phone } = args;
    const user = await this.findOne(userId);
    if (!user) {
      throw new Error(ErrorMessage.USER_NOT_FOUND);
    }
    if (!firstname.trim() && !lastname.trim() && !phone.trim()) {
      return user;
    }
    user.firstname = firstname.trim() || user.firstname;
    user.lastname = lastname.trim() || user.lastname;
    user.phone = phone.trim() || user.phone;
    await this.save(user);
    pubsub.publish('userInfoUpdated', { userInfoUpdated: user });
    return user;
  };
}
