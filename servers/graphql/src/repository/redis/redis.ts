import { RedisPubSub } from 'graphql-redis-subscriptions';
import Redis = require('ioredis');
import config from '../../config/config';
import { Subscription } from '../../generated/graphql';

export function createRedisConfig(isTLS: boolean): Redis.RedisOptions {
  if (isTLS) {
    return {
      password: config.redis.password,
      tls: {
        host: config.redis.host,
        port: config.redis.port,
      },
    };
  }
  return {
    host: config.redis.host,
    port: config.redis.port,
  };
}

const redisConfig = createRedisConfig(config.redis.useTls);

const redisPubSub = new RedisPubSub({
  publisher: new Redis(redisConfig),
  subscriber: new Redis(redisConfig),
});

export const pubsub: Omit<RedisPubSub, 'publish'> & {
  publish: <T extends keyof Subscription, PayloadType = unknown>(
    // eslint-disable-next-line no-unused-vars
    trigger: T,
    // eslint-disable-next-line no-unused-vars
    payload: PayloadType & Record<T, any>
  ) => Promise<void>;
} = redisPubSub;

export const redis = new Redis(redisConfig);
