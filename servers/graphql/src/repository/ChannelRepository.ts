import { EntityRepository, Repository } from 'typeorm';
import { TableName } from '../constant/TableName';
import { Channel } from '../entity/Channel';
import Context from '../types/Context';
import { pubsub } from './redis/redis';

@EntityRepository(Channel)
export class ChannelRepository extends Repository<Channel> {
  async removeChannel(channelId: number) {
    const channel = await this.findOne(channelId);
    if (!channel) {
      throw new Error('CHANNEL_NOT_FOUND');
    }
    if (!channel.removed) {
      channel.removed = 1;
      return this.save(channel);
    }
    return channel;
  }

  async findUserChannels(userId: number) {
    return this.createQueryBuilder('ch')
      .leftJoin(TableName.userChannel, 'uc', 'uc.channel_id = ch.id')
      .where('(uc.user_id = :userId OR NOT ch.is_private)', { userId })
      .getMany();
  }

  async addChannel(args: { isPublic: boolean; name: string; authorId: number }, ctx: Context) {
    const { isPublic, authorId, name } = args;
    if (!name.trim()) {
      throw new Error('EMPTY_NAME');
    }
    const newChannel = this.create({
      authorId,
      name: name.trim(),
      isPrivate: !isPublic,
    });

    const savedChannel = await this.save(newChannel);
    ctx.dataloader.channel.prime(savedChannel.id, savedChannel);
    pubsub.publish('channelAdded', {
      channelAdded: savedChannel,
    });
    await ctx.userChannel.addUserInChannel(savedChannel.id, authorId, ctx);
    return savedChannel;
  }
}
