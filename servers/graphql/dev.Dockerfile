FROM node:14-alpine as builder

WORKDIR /graphql

RUN apk update && apk upgrade && apk add --no-cache python make g++ git bash

COPY package.json /graphql/package.json
COPY yarn.lock /graphql

RUN yarn install

COPY . /graphql

EXPOSE 4000
EXPOSE 9229

RUN yarn run generate:type

ARG PG_PORT
ARG PG_HOST

CMD ./wait-for.sh ${PG_HOST}:${PG_PORT} -t 3000 -- echo "postgres is up" && yarn migrate &&  yarn start:dev
