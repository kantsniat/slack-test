<h1 align="left">Slack Clone</h1>
<p>
  <a href="#" target="_blank">
    <img alt="License: BSD" src="https://img.shields.io/badge/License-BSD-yellow.svg" />
  </a>
</p>

## Author

👤 **RAHARINALAMANGA Rovanirina Kanto**


> Slack clone is a chat application using React, Apollo-client, Apollo-server, jaeger, postgres and typeorm.

## Preview

<img src="capture.png" />



## Install

Copy servers/.env.example file to servers/.env:

```sh
cp servers/.env.example servers/.env && cp clients/web/.env.example clients/web/.env
```

Start the server:

```sh
cd servers && docker-compose up
```

Start the client :
```sh
cd clients/web && yarn && yarn start
```
## Listes des utilisateurs/mot de passe dans l'app: 

-  john123 (password1)
-  allegra (password2)
-  gui123 (password1)
-  camille123 (password4)
-  charles (password4)