import { BaseSubscriptionOptions } from '@apollo/client';
import { SubscribeToDiscussionMessageSubscription } from '../../generated/graphql';
import { updateCacheAfterAddingChannelMessage } from './addChannelMessage';
import { updateCacheAfterAddingDiscussionMessage } from './addMessage';

export const updateCacheAfterSubscribsriptionMessageAdded: BaseSubscriptionOptions<SubscribeToDiscussionMessageSubscription>['onSubscriptionData'] =
  ({ client, subscriptionData }) => {
    if (subscriptionData.data?.messageAdded) {
      const { messageAdded } = subscriptionData.data;
      const { channelId, userId } = messageAdded;
      if (channelId) {
        updateCacheAfterAddingChannelMessage(client as any, {
          data: { addChannelMessage: messageAdded },
        });
      } else if (userId) {
        updateCacheAfterAddingDiscussionMessage(client as any, {
          data: { addDiscussionMessage: messageAdded },
        });
      }
    }
  };
