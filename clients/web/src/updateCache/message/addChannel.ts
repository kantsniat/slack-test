import { BaseSubscriptionOptions, DataProxy, MutationUpdaterFn } from '@apollo/client';
import { AddChannelMutation, SubscribeToChannelAddedSubscription } from '../../generated/graphql';
import { getChannelsInCache } from './getChannelsInCache';

export const updateCacheAfterAddingChannel: MutationUpdaterFn<AddChannelMutation> = (
  cache,
  { data }
) => {
  if (data?.addChannel) {
    const { addChannel } = data;
    updateCacheCommon(cache, addChannel);
  }
};

export const updateCacheAfterSubscribsriptionChannelAdded: BaseSubscriptionOptions<SubscribeToChannelAddedSubscription>['onSubscriptionData'] =
  ({ client, subscriptionData }) => {
    if (subscriptionData.data?.channelAdded) {
      const { channelAdded } = subscriptionData.data;
      updateCacheCommon(client, channelAdded);
    }
  };

const updateCacheCommon = (cache: DataProxy, addChannel: AddChannelMutation['addChannel']) => {
  if (!addChannel) {
    return;
  }

  const { id } = addChannel;

  const { dataCache, queryVariable } = getChannelsInCache(cache);

  if (dataCache?.me.channels) {
    const { channels } = dataCache.me;

    if (!channels.some(ch => ch.id === id)) {
      const newChannels = [addChannel, ...channels];
      cache.writeQuery({
        ...queryVariable,
        data: {
          __typename: 'Query',
          me: {
            ...dataCache.me,
            channels: newChannels,
          },
        },
      });
    }
  }
};
