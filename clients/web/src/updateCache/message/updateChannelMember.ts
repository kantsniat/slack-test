import { BaseSubscriptionOptions, DataProxy } from '@apollo/client';
import { SubscribeToChannelMembersUpdatedSubscription } from '../../generated/graphql';
import { getChannelsInCache } from './getChannelsInCache';

export const updateCacheAfterSubscribsriptionChannelMemberAdded =
  (
    history: any,
    currentChannelId?: string | null
  ): BaseSubscriptionOptions<SubscribeToChannelMembersUpdatedSubscription>['onSubscriptionData'] =>
  ({ client, subscriptionData }) => {
    if (subscriptionData.data?.userChannelMemberUpdated) {
      const { userChannelMemberUpdated } = subscriptionData.data;
      updateCacheCommon(client, userChannelMemberUpdated, history, currentChannelId);
    }
  };

const updateCacheCommon = (
  cache: DataProxy,
  channel: SubscribeToChannelMembersUpdatedSubscription['userChannelMemberUpdated'],
  history: any,
  currentChannelId?: string | null
) => {
  if (!channel) {
    return;
  }

  const { members, ...channelInfos } = channel;

  const { dataCache, queryVariable } = getChannelsInCache(cache);
  if (dataCache?.me.channels) {
    const { channels } = dataCache.me;
    let newChannels: typeof channels | null = null;
    let redirect = false;
    if (!members.some(({ isMe }) => isMe)) {
      newChannels = channels.filter(ch => ch.id !== channelInfos.id);
      redirect = currentChannelId === channelInfos.id;
    } else if (!channels.some(ch => ch.id === channelInfos.id)) {
      newChannels = [channelInfos, ...channels];
    }
    if (newChannels) {
      cache.writeQuery({
        ...queryVariable,
        data: {
          __typename: 'Query',
          me: {
            ...dataCache.me,
            channels: [...newChannels],
          },
        },
      });
      if (redirect) {
        history.replace('/');
      }
    }
  }
};
