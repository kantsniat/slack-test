import { ApolloCache, MutationUpdaterFn } from '@apollo/client';
import { AddDiscussionMessageMutation } from '../../generated/graphql';
import { getMessagesInCacheFn } from './getMessageInCache';

export const updateCacheAfterAddingDiscussionMessage: MutationUpdaterFn<AddDiscussionMessageMutation> =
  (cache, { data }) => {
    if (data?.addDiscussionMessage) {
      const { addDiscussionMessage } = data;
      updateCacheCommon(cache, addDiscussionMessage);
    }
  };

const updateCacheCommon = (
  cache: ApolloCache<any>,
  addDiscussionMessage: AddDiscussionMessageMutation['addDiscussionMessage']
) => {
  if (!addDiscussionMessage) {
    return;
  }
  const { userId: userIdArg, authorId, isMine, id: messageId } = addDiscussionMessage;
  const userId = isMine ? userIdArg : authorId;

  if (!userId) {
    return;
  }

  const { dataCache, queryVariable } = getMessagesInCacheFn(cache, userId);

  if (dataCache?.me.messages) {
    const { messages, count } = dataCache.me.messages;

    if (!messages.some(ms => ms.id === messageId)) {
      const newMessages = [...messages, addDiscussionMessage];
      cache.writeQuery({
        ...queryVariable,
        data: {
          __typename: 'Query',
          me: {
            ...dataCache.me,
            messages: {
              ...dataCache.me.messages,
              count: count + 1,
              messages: newMessages,
            },
          },
        },
      });
    }
  }
};
