import { DataProxy } from '@apollo/client';
import { GetChannelMessagesQuery, GetChannelMessagesQueryVariables } from '../../generated/graphql';
import { GET_CHANNEL_MESSAGES } from '../../gql/channel/query';

export const getChannelMessagesInCacheFn = (cache: DataProxy, id: string) => {
  const queryVariable: DataProxy.Query<GetChannelMessagesQueryVariables, GetChannelMessagesQuery> =
    {
      query: GET_CHANNEL_MESSAGES,
      variables: {
        channelId: id,
        filter: {},
      },
    };
  return { dataCache: cache.readQuery(queryVariable), queryVariable };
};
