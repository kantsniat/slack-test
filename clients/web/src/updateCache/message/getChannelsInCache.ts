import { DataProxy } from '@apollo/client';
import { GetAllUserChannelQuery, GetAllUserChannelQueryVariables } from '../../generated/graphql';
import { GET_ALL_USER_CHANNEL } from '../../gql/user/query';

export const getChannelsInCache = (cache: DataProxy) => {
  const queryVariable: DataProxy.Query<GetAllUserChannelQueryVariables, GetAllUserChannelQuery> = {
    query: GET_ALL_USER_CHANNEL,
  };
  return { dataCache: cache.readQuery(queryVariable), queryVariable };
};
