import { DataProxy } from '@apollo/client';
import { GetUserMessageQuery, GetUserMessageQueryVariables } from '../../generated/graphql';
import { GET_USER_MESSAGES } from '../../gql/user/query';

export const getMessagesInCacheFn = (cache: DataProxy, id: string) => {
  const queryVariable: DataProxy.Query<GetUserMessageQueryVariables, GetUserMessageQuery> = {
    query: GET_USER_MESSAGES,
    variables: {
      filter: {
        userId: id,
      },
    },
  };
  return { dataCache: cache.readQuery(queryVariable), queryVariable };
};
