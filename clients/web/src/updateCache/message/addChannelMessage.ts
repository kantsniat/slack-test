import { ApolloCache, MutationUpdaterFn } from '@apollo/client';
import { AddChannelMessageMutation } from '../../generated/graphql';
import { getChannelMessagesInCacheFn } from './getChannelMessageInCache';

export const updateCacheAfterAddingChannelMessage: MutationUpdaterFn<AddChannelMessageMutation> = (
  cache,
  { data }
) => {
  if (data?.addChannelMessage) {
    const { addChannelMessage } = data;
    updateCacheCommon(cache, addChannelMessage);
  }
};

const updateCacheCommon = (
  cache: ApolloCache<any>,
  addChannelMessage: AddChannelMessageMutation['addChannelMessage']
) => {
  if (!addChannelMessage) {
    return;
  }
  const { channelId, id: messageId } = addChannelMessage;

  if (!channelId) {
    return;
  }

  const { dataCache, queryVariable } = getChannelMessagesInCacheFn(cache, channelId);

  if (dataCache?.channel.messages) {
    const { messages, count } = dataCache.channel.messages;

    if (!messages.some(ms => ms.id === messageId)) {
      const newMessages = [...messages, addChannelMessage];
      cache.writeQuery({
        ...queryVariable,
        data: {
          __typename: 'Query',
          channel: {
            ...dataCache.channel,
            messages: {
              ...dataCache.channel.messages,
              count: count + 1,
              messages: newMessages,
            },
          },
        },
      });
    }
  }
};
