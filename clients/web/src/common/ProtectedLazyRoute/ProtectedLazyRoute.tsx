import { FC, Suspense } from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import authService from '../../services/authServices';
import { Loading } from '../Loading';

type AccessType = 'private' | 'public' | 'authentication';

export interface ProtectedLazyRouteProps extends RouteProps {
  noAccessRedirection?: string;
  fallbackComponent?: FC;
  access?: AccessType;
}

const defaultFallback = <Loading isFullHeight={true} />;

const getNoAccessDefaultPath = (access: AccessType): string | undefined => {
  const isConnected = Boolean(authService.getAccessToken());
  switch (access) {
    case 'authentication':
      return isConnected ? '/' : undefined;
    case 'private':
      return isConnected ? undefined : '/login';
    default:
      return undefined;
  }
};

const ProtectedLazyRoute: FC<ProtectedLazyRouteProps> = props => {
  let redirectionPath = '';
  const { noAccessRedirection, component, fallbackComponent, access: accessProps } = props;
  const FallbackComponent = fallbackComponent || defaultFallback;
  const access: AccessType = accessProps || 'private';

  const pathToRedirect = getNoAccessDefaultPath(access);

  if (pathToRedirect) {
    redirectionPath = noAccessRedirection || pathToRedirect;
  }

  if (redirectionPath === '') {
    return (
      <Suspense fallback={FallbackComponent}>
        <Route {...props} component={component as any} />
      </Suspense>
    );
  }

  return (
    <Suspense fallback={FallbackComponent}>
      <Redirect to={{ pathname: redirectionPath }} />
    </Suspense>
  );
};

export default ProtectedLazyRoute;
