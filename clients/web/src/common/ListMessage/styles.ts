import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(() => ({
  containerListMessage: {
    padding: '14px 21px',
    overflowY: 'auto',
    height: '100%',
  },
  messages: {
    marginTop: 28,
  },
}));
