import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(theme => ({
  containerMessageItem: {
    ...theme.containerStyles.flexRow,
  },
  messageContainer: {
    marginLeft: 7,
    ...theme.containerStyles.flexColumn,
  },
  authorContainer: {
    ...theme.containerStyles.flexRow,
    alignItems: 'center',
  },
  time: {
    marginLeft: 21,
    fontStyle: 'italic',
    marginTop: 2,
  },
}));
