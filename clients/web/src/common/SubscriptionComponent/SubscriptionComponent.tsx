import { FC } from 'react';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import {
  useSubscribeToChannelAddedSubscription,
  useSubscribeToChannelMembersUpdatedSubscription,
  useSubscribeToDiscussionMessageSubscription,
  useSubscribeToUserInfosUpdatedSubscription,
  useSubscribeToUserLoggedInSubscription,
  useSubscribeToUserLoggedOutSubscription,
} from '../../generated/graphql';
import { updateCacheAfterSubscribsriptionChannelAdded } from '../../updateCache/message/addChannel';
import { updateCacheAfterSubscribsriptionMessageAdded } from '../../updateCache/message/subscriptionMessageAdded';
import { updateCacheAfterSubscribsriptionChannelMemberAdded } from '../../updateCache/message/updateChannelMember';
import { isChannelFn } from '../../utils/locationUtils';

const SubscriptionComponent: FC = () => {
  const history = useHistory();
  const { id } = useParams<{ id?: string }>();
  const { pathname } = useLocation();
  const isChannel = isChannelFn(pathname);
  // User
  useSubscribeToUserLoggedOutSubscription();
  useSubscribeToUserLoggedInSubscription();
  useSubscribeToUserInfosUpdatedSubscription();
  // Channel
  useSubscribeToChannelAddedSubscription({
    onSubscriptionData: updateCacheAfterSubscribsriptionChannelAdded,
  });
  useSubscribeToChannelMembersUpdatedSubscription({
    onSubscriptionData: updateCacheAfterSubscribsriptionChannelMemberAdded(
      history,
      isChannel ? id : null
    ),
  });
  // Message
  useSubscribeToDiscussionMessageSubscription({
    onSubscriptionData: updateCacheAfterSubscribsriptionMessageAdded,
  });
  return null;
};

export default SubscriptionComponent;
