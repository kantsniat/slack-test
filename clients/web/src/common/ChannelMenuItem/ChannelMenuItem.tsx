import { Typography } from '@material-ui/core';
import { FC } from 'react';
import { useHistory } from 'react-router-dom';
import { CustomListItem } from '../CustomListItem';
import useStyle from './styles';

interface ChannelMenuItemsProps {
  name: string;
  id: number;
  isActive: boolean;
  isPrivate: boolean;
}

const ChannelMenuItem: FC<ChannelMenuItemsProps> = props => {
  const { name, id, isActive, isPrivate } = props;
  const history = useHistory();
  const classes = useStyle();

  const onClick = () => history.push(`/channel/${id}`);

  return (
    <CustomListItem
      selected={isActive}
      className={classes.containerChannelMenuItem}
      onClick={onClick}
    >
      <Typography color={!isPrivate && !isActive ? 'inherit' : undefined} variant="body2">
        # {name}
      </Typography>
    </CustomListItem>
  );
};

export default ChannelMenuItem;
