import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(() => ({
  containerChannelMenuItem: {
    width: '100%',
    paddingLeft: 30,
  },
}));
