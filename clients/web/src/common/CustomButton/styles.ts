import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(() => ({
  containerCustomButton: {},
  text: {
    fontWeight: 600,
  },
  loading: {
    marginLeft: 7,
  },
}));
