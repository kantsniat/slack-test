import { Typography, TypographyProps } from '@material-ui/core';
import fromDistance from 'date-fns/formatDistance';
import { FC } from 'react';

interface TimeAgoProps {
  dateTime: Date;
}

const TimeAgo: FC<TimeAgoProps & TypographyProps> = props => {
  const { dateTime, ...otherProps } = props;
  const fromAgo = fromDistance(dateTime, new Date());
  return (
    <Typography variant="caption" {...otherProps}>
      {fromAgo}
    </Typography>
  );
};

export default TimeAgo;
