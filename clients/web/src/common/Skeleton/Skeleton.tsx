import { withStyles } from '@material-ui/core/styles';
import { Skeleton as SkeletonLab } from '@material-ui/lab';

const Skeleton = withStyles({
  root: {
    backgroundColor: '#fff',
  },
})(SkeletonLab);

export default Skeleton;
