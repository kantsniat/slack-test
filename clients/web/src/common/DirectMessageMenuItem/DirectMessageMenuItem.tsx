import { FC } from 'react';
import { useHistory } from 'react-router-dom';
import { CustomListItem } from '../CustomListItem';
import { Indicator } from '../OnlineIndicator/Indicator';
import useStyle from './styles';

interface DirectMessageMenuItemsProps {
  userName: string;
  userId: number;
  isActive: boolean;
  isOnline: boolean;
}

const DirectMessageMenuItem: FC<DirectMessageMenuItemsProps> = props => {
  const { userName, userId: id, isActive, isOnline } = props;
  const history = useHistory();
  const classes = useStyle();

  const onClick = () => history.push(`/message/${id}`);

  return (
    <CustomListItem
      selected={isActive}
      className={classes.containerDirectMessageMenuItem}
      onClick={onClick}
    >
      <span className={classes.indicator}>
        {!isOnline && <p className={classes.margin} />}
        {isOnline && <Indicator size={10} isOffline={false} />}
        {userName}
      </span>
    </CustomListItem>
  );
};

export default DirectMessageMenuItem;
