import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(theme => ({
  containerErrorComponent: {
    ...theme.containerStyles.flexRow,
    alignItems: 'center',
    justifyContent: 'center',
  },
  fullWidth: {
    height: '100%',
    width: '100%',
  },
}));
