import { Theme } from '@material-ui/core';
import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles((theme: Theme) => ({
  containerLoading: {
    alignSelf: 'center',
  },
  containerLoadingFullHeight: {
    height: '100%',
    width: '100%',
    ...theme.containerStyles.flexColumn,
    alignItems: 'center',
    justifyContent: 'center',
  },
}));
