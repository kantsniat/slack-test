import { gql } from '@apollo/client';
import { MESSAGE_FRAGMENT } from './fragment';

export const ADD_DISCUSSION_MESSAGE = gql`
  mutation AddDiscussionMessage($input: AddDiscussionMessageInput!) {
    addDiscussionMessage(input: $input) {
      ...MessageInfos
    }
  }
  ${MESSAGE_FRAGMENT}
`;

export const ADD_CHANNEL_MESSAGE = gql`
  mutation AddChannelMessage($input: AddChannelMessageInput!) {
    addChannelMessage(input: $input) {
      ...MessageInfos
    }
  }
  ${MESSAGE_FRAGMENT}
`;
