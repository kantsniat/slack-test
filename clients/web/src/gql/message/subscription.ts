import { gql } from '@apollo/client';
import { MESSAGE_FRAGMENT } from './fragment';

export const SUBSCRIBE_TO_DISCUSSION_MESSAGE_ADDED = gql`
  subscription SubscribeToDiscussionMessage {
    messageAdded {
      ...MessageInfos
    }
  }
  ${MESSAGE_FRAGMENT}
`;
