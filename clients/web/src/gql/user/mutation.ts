import { gql } from '@apollo/client';
import { USER_BASIC_FRAGMENT } from './fragment';

export const UPDATE_USER = gql`
  mutation UpdateUser($input: UpdateUserInput!) {
    updateUser(input: $input) {
      ...UserBasicInfos
    }
  }
  ${USER_BASIC_FRAGMENT}
`;
