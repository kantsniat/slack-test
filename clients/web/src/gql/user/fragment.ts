import { gql } from '@apollo/client';

export const USER_BASIC_FRAGMENT = gql`
  fragment UserBasicInfos on User {
    id
    firstname
    lastname
    username
    phone
    lastConnection
    isMe
  }
`;
