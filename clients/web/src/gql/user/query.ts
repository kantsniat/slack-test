import { gql } from '@apollo/client';
import { CHANNEL_FRAGMENT } from '../channel/fragment';
import { MESSAGE_FRAGMENT } from '../message/fragment';
import { USER_BASIC_FRAGMENT } from './fragment';

export const ME = gql`
  query Me {
    me {
      ...UserBasicInfos
    }
  }
  ${USER_BASIC_FRAGMENT}
`;

// Get listUser
export const GET_ALL_USERS = gql`
  query GetAllUsers {
    me {
      id
      users {
        ...UserBasicInfos
      }
    }
  }
  ${USER_BASIC_FRAGMENT}
`;

// UserMessages
export const GET_USER_MESSAGES = gql`
  query GetUserMessage($filter: UserMessagesInput!) {
    me {
      id
      messages(filter: $filter) {
        userId
        user {
          ...UserBasicInfos
        }
        count
        messages {
          ...MessageInfos
        }
      }
    }
  }
  ${USER_BASIC_FRAGMENT}
  ${MESSAGE_FRAGMENT}
`;

// Get listUser
export const GET_ALL_USER_CHANNEL = gql`
  query GetAllUserChannel {
    me {
      id
      channels {
        ...ChannelInfos
      }
    }
  }
  ${CHANNEL_FRAGMENT}
`;
