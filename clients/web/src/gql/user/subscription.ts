import { gql } from '@apollo/client';
import { USER_BASIC_FRAGMENT } from './fragment';

export const SUBSCRIBE_TO_USER_LOGGED_IN = gql`
  subscription SubscribeToUserLoggedIn {
    userLoggedIn {
      id
      lastConnection
    }
  }
`;

export const SUBSCRIBE_TO_USER_LOGGED_OUT = gql`
  subscription SubscribeToUserLoggedOut {
    userLoggedOut {
      id
      lastConnection
    }
  }
  ${USER_BASIC_FRAGMENT}
`;

export const SUBSCRIBE_TO_USER_Infos_UPDATED = gql`
  subscription SubscribeToUserInfosUpdated {
    userInfoUpdated {
      ...UserBasicInfos
    }
  }
  ${USER_BASIC_FRAGMENT}
`;
