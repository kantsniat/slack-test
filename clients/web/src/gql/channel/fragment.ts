import { gql } from '@apollo/client';
import { MESSAGE_FRAGMENT } from '../message/fragment';

export const CHANNEL_FRAGMENT = gql`
  fragment ChannelInfos on Channel {
    id
    name
    authorId
    createdAt
    removed
    isAuthor
    isPrivate
  }
`;

export const CHANNEL_MESSAGE_FRAGMENT = gql`
  fragment ChannelMessageInfos on ChannelMessages {
    channelId
    count
    messages {
      ...MessageInfos
    }
  }
  ${MESSAGE_FRAGMENT}
`;
