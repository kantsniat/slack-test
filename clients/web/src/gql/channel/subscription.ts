import { gql } from '@apollo/client';
import { USER_BASIC_FRAGMENT } from '../user/fragment';
import { CHANNEL_FRAGMENT } from './fragment';

export const SUBSCRIBE_TO_CHANNEL_ADDED = gql`
  subscription SubscribeToChannelAdded {
    channelAdded {
      ...ChannelInfos
    }
  }
  ${CHANNEL_FRAGMENT}
`;

export const SUBSCRIBE_TO_CHANNEL_MEMBERS_UPDATED = gql`
  subscription SubscribeToChannelMembersUpdated {
    userChannelMemberUpdated {
      ...ChannelInfos
      members {
        ...UserBasicInfos
      }
    }
  }
  ${CHANNEL_FRAGMENT}
  ${USER_BASIC_FRAGMENT}
`;
