import { gql } from '@apollo/client';
import { USER_BASIC_FRAGMENT } from '../user/fragment';
import { CHANNEL_FRAGMENT } from './fragment';

export const MUTATION_ADD_CHANNEL = gql`
  mutation AddChannel($input: AddChannelInput!) {
    addChannel(input: $input) {
      ...ChannelInfos
    }
  }
  ${CHANNEL_FRAGMENT}
`;

export const MUTATION_ADD_CHANNEL_PARTICIPANTS = gql`
  mutation AddChannelParticipants($input: AddChannelParticipantsInput!) {
    addChannelParticipants(input: $input) {
      ...ChannelInfos
      members {
        ...UserBasicInfos
      }
    }
  }
  ${USER_BASIC_FRAGMENT}
  ${CHANNEL_FRAGMENT}
`;

export const MUTATION_REMOVE_CHANNEL_PARTICIPANTS = gql`
  mutation RemoveChannelParticipants($input: RemoveChannelParticipantsInput!) {
    removeChannelParticipants(input: $input) {
      ...ChannelInfos
      members {
        ...UserBasicInfos
      }
    }
  }
  ${USER_BASIC_FRAGMENT}
  ${CHANNEL_FRAGMENT}
`;
