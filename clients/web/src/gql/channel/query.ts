import { gql } from '@apollo/client';
import { USER_BASIC_FRAGMENT } from '../user/fragment';
import { CHANNEL_FRAGMENT, CHANNEL_MESSAGE_FRAGMENT } from './fragment';

export const GET_CHANNEL_MESSAGES = gql`
  query GetChannelMessages($channelId: ID!, $filter: ChannelMessagesInput!) {
    channel(channelId: $channelId) {
      ...ChannelInfos
      messages(filter: $filter) {
        ...ChannelMessageInfos
      }
    }
  }
  ${CHANNEL_FRAGMENT}
  ${CHANNEL_MESSAGE_FRAGMENT}
`;

export const GET_CHANNEL_MEMBERS = gql`
  query GetChannelMembers($channelId: ID!) {
    channel(channelId: $channelId) {
      id
      isPrivate
      isAuthor
      members {
        ...UserBasicInfos
      }
    }
  }
  ${USER_BASIC_FRAGMENT}
`;
