// eslint-disable-next-line import/no-extraneous-dependencies
import { ComponentStory } from '@storybook/react';
import { CustomListItem } from '../common/CustomListItem';

export default {
  title: 'CustomListItem',
  component: CustomListItem,
};

const Template: ComponentStory<typeof CustomListItem> = args => <CustomListItem {...args} />;

export const Main = Template.bind({});

Main.args = {
  selected: true,
  children: '#Child',
};
