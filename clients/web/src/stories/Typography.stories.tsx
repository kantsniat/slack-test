import { Typography } from '@material-ui/core';
// eslint-disable-next-line import/no-extraneous-dependencies
import { ComponentStory } from '@storybook/react';

export default {
  title: 'Typography',
  component: Typography,
};

const Template: ComponentStory<typeof Typography> = args => (
  <Typography {...args}>Title</Typography>
);

export const H1 = Template.bind({});
export const H2 = Template.bind({});
export const H3 = Template.bind({});
export const H4 = Template.bind({});
export const H5 = Template.bind({});
export const H6 = Template.bind({});
export const Body1 = Template.bind({});
export const Body2 = Template.bind({});
export const Caption = Template.bind({});
export const Subtitle1 = Template.bind({});
export const Subtitle2 = Template.bind({});

H1.args = {
  color: 'primary',
  variant: 'h1',
};

H2.args = {
  color: 'primary',
  variant: 'h2',
};

H3.args = {
  color: 'primary',
  variant: 'h3',
};

H4.args = {
  color: 'primary',
  variant: 'h4',
};

H5.args = {
  color: 'primary',
  variant: 'h5',
};

H6.args = {
  color: 'primary',
  variant: 'h6',
};

Body1.args = {
  color: 'primary',
  variant: 'body1',
};

Body2.args = {
  color: 'primary',
  variant: 'body2',
};

Caption.args = {
  color: 'primary',
  variant: 'caption',
};

Subtitle1.args = {
  color: 'primary',
  variant: 'subtitle1',
};

Subtitle2.args = {
  color: 'primary',
  variant: 'subtitle2',
};
