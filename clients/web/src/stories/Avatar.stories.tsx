// eslint-disable-next-line import/no-extraneous-dependencies
import { ComponentStory } from '@storybook/react';
import { Avatar } from '../common/Avatar';

export default {
  title: 'Avatar',
  component: Avatar,
};

const Template: ComponentStory<typeof Avatar> = args => <Avatar {...args} />;

export const Main = Template.bind({});

Main.args = {
  userName: 'vf',
  indicatorStatus: 'isMine',
  size: 54,
};
