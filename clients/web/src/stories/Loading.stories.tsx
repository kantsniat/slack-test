// eslint-disable-next-line import/no-extraneous-dependencies
import { ComponentStory, storiesOf } from '@storybook/react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { muiTheme } from 'storybook-addon-material-ui';
import { Loading } from '../common/Loading';
import myTheme from '../themes/myTheme';

export default {
  title: 'Loading',
  component: Loading,
};

storiesOf('Material-UI', module).addDecorator(muiTheme([myTheme]));

const Template: ComponentStory<typeof Loading> = args => <Loading {...args} />;

export const Main = () => <Loading />;

export const FullHeight = Template.bind({});

FullHeight.args = {
  isFullHeight: true,
};
