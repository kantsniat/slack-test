// eslint-disable-next-line import/no-extraneous-dependencies
import { ComponentStory } from '@storybook/react';
import { MessageItem } from '../common/MessageItem';

export default {
  title: 'MessageItem',
  component: MessageItem,
};

const Template: ComponentStory<typeof MessageItem> = args => <MessageItem {...args} />;

export const Main = Template.bind({});

Main.args = {
  message: {
    id: '1',
    text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus velit error, vero natus earum ipsam quo nemo, modi doloremque nihil aut voluptate voluptas non excepturi quod at sint asperiores assumenda?',
    createdAt: new Date().toISOString(),
    isMine: false,
    removed: false,
    authorId: '1',
    userId: '3',
    author: {
      id: '1',
      firstname: 'Firstname',
      lastname: 'Name',
      lastConnection: 0,
      isMe: false,
      phone: '+261349772724',
      username: 'user',
    },
  },
};
