// eslint-disable-next-line import/no-extraneous-dependencies
import { ComponentStory } from '@storybook/react';
import { CustomButton } from '../common/CustomButton';

export default {
  title: 'CustomButton',
  component: CustomButton,
};

const Template: ComponentStory<typeof CustomButton> = args => <CustomButton {...args} />;

export const Button = () => <CustomButton>Button</CustomButton>;

export const LoadingButton = Template.bind({});

LoadingButton.args = {
  isLoading: false,
  children: 'Slack button',
  color: 'primary',
};
