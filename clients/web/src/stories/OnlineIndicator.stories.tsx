// eslint-disable-next-line import/no-extraneous-dependencies
import { ComponentStory } from '@storybook/react';
import { OnlineIndicator } from '../common/OnlineIndicator';

export default {
  title: 'OnlineIndicator',
  component: OnlineIndicator,
};

const Template: ComponentStory<typeof OnlineIndicator> = args => <OnlineIndicator {...args} />;

export const Main = Template.bind({});

Main.args = {
  status: 'isMine',
};
