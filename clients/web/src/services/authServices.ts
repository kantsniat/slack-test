const ACCESS_TOKEN_KEY = btoa('ACCESS_TOKEN');

const authService = {
  getAccessToken: () => {
    const token = localStorage.getItem(ACCESS_TOKEN_KEY);
    return token;
  },
  setAccessToken: (token: string) => {
    localStorage.setItem(ACCESS_TOKEN_KEY, token);
  },
  clear: () => localStorage.clear(),
};

export default authService;
