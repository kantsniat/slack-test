/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable no-use-before-define */
import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';

export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {};
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** A date-time string at UTC, such as 2007-12-03T10:15:30Z, compliant with the `date-time` format outlined in section 5.6 of the RFC 3339 profile of the ISO 8601 standard for representation of dates and times using the Gregorian calendar. */
  DateTime: any;
};

export type AddChannelInput = {
  isPublic: Scalars['Boolean'];
  name: Scalars['String'];
};

export type AddChannelMessageInput = {
  channelId: Scalars['ID'];
  text: Scalars['String'];
};

export type AddChannelParticipantsInput = {
  channelId: Scalars['ID'];
  userId: Scalars['ID'];
};

export type AddDiscussionMessageInput = {
  text: Scalars['String'];
  userId: Scalars['ID'];
};

export type Channel = {
  __typename?: 'Channel';
  author: User;
  authorId: Scalars['ID'];
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  isAuthor: Scalars['Boolean'];
  isPrivate: Scalars['Boolean'];
  members: Array<User>;
  messages: ChannelMessages;
  name: Scalars['String'];
  removed: Scalars['Boolean'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type ChannelMessagesArgs = {
  filter: ChannelMessagesInput;
};

export type ChannelMessages = {
  __typename?: 'ChannelMessages';
  channel: Channel;
  channelId: Scalars['ID'];
  count: Scalars['Int'];
  messages: Array<Message>;
};

export type ChannelMessagesInput = {
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
};

export type Destination = Channel | User;

export type DiscussionMessages = {
  __typename?: 'DiscussionMessages';
  count: Scalars['Int'];
  messages: Array<Message>;
  user: User;
  userId: Scalars['ID'];
};

export type LoginInput = {
  password: Scalars['String'];
  username: Scalars['String'];
};

export type Message = {
  __typename?: 'Message';
  author: User;
  authorId: Scalars['ID'];
  channelId?: Maybe<Scalars['ID']>;
  createdAt: Scalars['DateTime'];
  destination: Destination;
  id: Scalars['ID'];
  isMine: Scalars['Boolean'];
  removed: Scalars['Boolean'];
  text: Scalars['String'];
  updatedAt?: Maybe<Scalars['DateTime']>;
  userId?: Maybe<Scalars['ID']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  addChannel: Channel;
  addChannelMessage: Message;
  addChannelParticipants: Channel;
  addDiscussionMessage: Message;
  login: Scalars['String'];
  removeChannel: Channel;
  removeChannelParticipants: Channel;
  updateUser: User;
};

export type MutationAddChannelArgs = {
  input: AddChannelInput;
};

export type MutationAddChannelMessageArgs = {
  input: AddChannelMessageInput;
};

export type MutationAddChannelParticipantsArgs = {
  input: AddChannelParticipantsInput;
};

export type MutationAddDiscussionMessageArgs = {
  input: AddDiscussionMessageInput;
};

export type MutationLoginArgs = {
  input: LoginInput;
};

export type MutationRemoveChannelArgs = {
  channelId: Scalars['ID'];
};

export type MutationRemoveChannelParticipantsArgs = {
  input: RemoveChannelParticipantsInput;
};

export type MutationUpdateUserArgs = {
  input: UpdateUserInput;
};

export type Query = {
  __typename?: 'Query';
  channel: Channel;
  me: User;
};

export type QueryChannelArgs = {
  channelId: Scalars['ID'];
};

export type RemoveChannelParticipantsInput = {
  channelId: Scalars['ID'];
  userId: Scalars['ID'];
};

export type Subscription = {
  __typename?: 'Subscription';
  channelAdded: Channel;
  messageAdded: Message;
  userChannelMemberUpdated: Channel;
  userInfoUpdated: User;
  userLoggedIn: User;
  userLoggedOut: User;
};

export type UpdateUserInput = {
  firstname: Scalars['String'];
  lastname: Scalars['String'];
  phone: Scalars['String'];
};

export type User = {
  __typename?: 'User';
  channels: Array<Channel>;
  createdAt: Scalars['DateTime'];
  firstname: Scalars['String'];
  id: Scalars['ID'];
  isMe: Scalars['Boolean'];
  lastConnection?: Maybe<Scalars['Int']>;
  lastname: Scalars['String'];
  messages: DiscussionMessages;
  phone: Scalars['String'];
  updatedAt?: Maybe<Scalars['DateTime']>;
  username: Scalars['String'];
  users: Array<User>;
};

export type UserMessagesArgs = {
  filter: UserMessagesInput;
};

export type UserMessagesInput = {
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  userId: Scalars['ID'];
};

export type LoginMutationVariables = Exact<{
  input: LoginInput;
}>;

export type LoginMutation = { __typename?: 'Mutation'; login: string };

export type ChannelInfosFragment = {
  __typename?: 'Channel';
  id: string;
  name: string;
  authorId: string;
  createdAt: any;
  removed: boolean;
  isAuthor: boolean;
  isPrivate: boolean;
};

export type ChannelMessageInfosFragment = {
  __typename?: 'ChannelMessages';
  channelId: string;
  count: number;
  messages: Array<{
    __typename?: 'Message';
    id: string;
    text: string;
    createdAt: any;
    isMine: boolean;
    removed: boolean;
    userId?: string | null | undefined;
    authorId: string;
    channelId?: string | null | undefined;
    author: {
      __typename?: 'User';
      id: string;
      firstname: string;
      lastname: string;
      username: string;
      phone: string;
      lastConnection?: number | null | undefined;
      isMe: boolean;
    };
  }>;
};

export type AddChannelMutationVariables = Exact<{
  input: AddChannelInput;
}>;

export type AddChannelMutation = {
  __typename?: 'Mutation';
  addChannel: {
    __typename?: 'Channel';
    id: string;
    name: string;
    authorId: string;
    createdAt: any;
    removed: boolean;
    isAuthor: boolean;
    isPrivate: boolean;
  };
};

export type AddChannelParticipantsMutationVariables = Exact<{
  input: AddChannelParticipantsInput;
}>;

export type AddChannelParticipantsMutation = {
  __typename?: 'Mutation';
  addChannelParticipants: {
    __typename?: 'Channel';
    id: string;
    name: string;
    authorId: string;
    createdAt: any;
    removed: boolean;
    isAuthor: boolean;
    isPrivate: boolean;
    members: Array<{
      __typename?: 'User';
      id: string;
      firstname: string;
      lastname: string;
      username: string;
      phone: string;
      lastConnection?: number | null | undefined;
      isMe: boolean;
    }>;
  };
};

export type RemoveChannelParticipantsMutationVariables = Exact<{
  input: RemoveChannelParticipantsInput;
}>;

export type RemoveChannelParticipantsMutation = {
  __typename?: 'Mutation';
  removeChannelParticipants: {
    __typename?: 'Channel';
    id: string;
    name: string;
    authorId: string;
    createdAt: any;
    removed: boolean;
    isAuthor: boolean;
    isPrivate: boolean;
    members: Array<{
      __typename?: 'User';
      id: string;
      firstname: string;
      lastname: string;
      username: string;
      phone: string;
      lastConnection?: number | null | undefined;
      isMe: boolean;
    }>;
  };
};

export type GetChannelMessagesQueryVariables = Exact<{
  channelId: Scalars['ID'];
  filter: ChannelMessagesInput;
}>;

export type GetChannelMessagesQuery = {
  __typename?: 'Query';
  channel: {
    __typename?: 'Channel';
    id: string;
    name: string;
    authorId: string;
    createdAt: any;
    removed: boolean;
    isAuthor: boolean;
    isPrivate: boolean;
    messages: {
      __typename?: 'ChannelMessages';
      channelId: string;
      count: number;
      messages: Array<{
        __typename?: 'Message';
        id: string;
        text: string;
        createdAt: any;
        isMine: boolean;
        removed: boolean;
        userId?: string | null | undefined;
        authorId: string;
        channelId?: string | null | undefined;
        author: {
          __typename?: 'User';
          id: string;
          firstname: string;
          lastname: string;
          username: string;
          phone: string;
          lastConnection?: number | null | undefined;
          isMe: boolean;
        };
      }>;
    };
  };
};

export type GetChannelMembersQueryVariables = Exact<{
  channelId: Scalars['ID'];
}>;

export type GetChannelMembersQuery = {
  __typename?: 'Query';
  channel: {
    __typename?: 'Channel';
    id: string;
    isPrivate: boolean;
    isAuthor: boolean;
    members: Array<{
      __typename?: 'User';
      id: string;
      firstname: string;
      lastname: string;
      username: string;
      phone: string;
      lastConnection?: number | null | undefined;
      isMe: boolean;
    }>;
  };
};

export type SubscribeToChannelAddedSubscriptionVariables = Exact<{ [key: string]: never }>;

export type SubscribeToChannelAddedSubscription = {
  __typename?: 'Subscription';
  channelAdded: {
    __typename?: 'Channel';
    id: string;
    name: string;
    authorId: string;
    createdAt: any;
    removed: boolean;
    isAuthor: boolean;
    isPrivate: boolean;
  };
};

export type SubscribeToChannelMembersUpdatedSubscriptionVariables = Exact<{ [key: string]: never }>;

export type SubscribeToChannelMembersUpdatedSubscription = {
  __typename?: 'Subscription';
  userChannelMemberUpdated: {
    __typename?: 'Channel';
    id: string;
    name: string;
    authorId: string;
    createdAt: any;
    removed: boolean;
    isAuthor: boolean;
    isPrivate: boolean;
    members: Array<{
      __typename?: 'User';
      id: string;
      firstname: string;
      lastname: string;
      username: string;
      phone: string;
      lastConnection?: number | null | undefined;
      isMe: boolean;
    }>;
  };
};

export type MessageInfosFragment = {
  __typename?: 'Message';
  id: string;
  text: string;
  createdAt: any;
  isMine: boolean;
  removed: boolean;
  userId?: string | null | undefined;
  authorId: string;
  channelId?: string | null | undefined;
  author: {
    __typename?: 'User';
    id: string;
    firstname: string;
    lastname: string;
    username: string;
    phone: string;
    lastConnection?: number | null | undefined;
    isMe: boolean;
  };
};

export type AddDiscussionMessageMutationVariables = Exact<{
  input: AddDiscussionMessageInput;
}>;

export type AddDiscussionMessageMutation = {
  __typename?: 'Mutation';
  addDiscussionMessage: {
    __typename?: 'Message';
    id: string;
    text: string;
    createdAt: any;
    isMine: boolean;
    removed: boolean;
    userId?: string | null | undefined;
    authorId: string;
    channelId?: string | null | undefined;
    author: {
      __typename?: 'User';
      id: string;
      firstname: string;
      lastname: string;
      username: string;
      phone: string;
      lastConnection?: number | null | undefined;
      isMe: boolean;
    };
  };
};

export type AddChannelMessageMutationVariables = Exact<{
  input: AddChannelMessageInput;
}>;

export type AddChannelMessageMutation = {
  __typename?: 'Mutation';
  addChannelMessage: {
    __typename?: 'Message';
    id: string;
    text: string;
    createdAt: any;
    isMine: boolean;
    removed: boolean;
    userId?: string | null | undefined;
    authorId: string;
    channelId?: string | null | undefined;
    author: {
      __typename?: 'User';
      id: string;
      firstname: string;
      lastname: string;
      username: string;
      phone: string;
      lastConnection?: number | null | undefined;
      isMe: boolean;
    };
  };
};

export type SubscribeToDiscussionMessageSubscriptionVariables = Exact<{ [key: string]: never }>;

export type SubscribeToDiscussionMessageSubscription = {
  __typename?: 'Subscription';
  messageAdded: {
    __typename?: 'Message';
    id: string;
    text: string;
    createdAt: any;
    isMine: boolean;
    removed: boolean;
    userId?: string | null | undefined;
    authorId: string;
    channelId?: string | null | undefined;
    author: {
      __typename?: 'User';
      id: string;
      firstname: string;
      lastname: string;
      username: string;
      phone: string;
      lastConnection?: number | null | undefined;
      isMe: boolean;
    };
  };
};

export type UserBasicInfosFragment = {
  __typename?: 'User';
  id: string;
  firstname: string;
  lastname: string;
  username: string;
  phone: string;
  lastConnection?: number | null | undefined;
  isMe: boolean;
};

export type UpdateUserMutationVariables = Exact<{
  input: UpdateUserInput;
}>;

export type UpdateUserMutation = {
  __typename?: 'Mutation';
  updateUser: {
    __typename?: 'User';
    id: string;
    firstname: string;
    lastname: string;
    username: string;
    phone: string;
    lastConnection?: number | null | undefined;
    isMe: boolean;
  };
};

export type MeQueryVariables = Exact<{ [key: string]: never }>;

export type MeQuery = {
  __typename?: 'Query';
  me: {
    __typename?: 'User';
    id: string;
    firstname: string;
    lastname: string;
    username: string;
    phone: string;
    lastConnection?: number | null | undefined;
    isMe: boolean;
  };
};

export type GetAllUsersQueryVariables = Exact<{ [key: string]: never }>;

export type GetAllUsersQuery = {
  __typename?: 'Query';
  me: {
    __typename?: 'User';
    id: string;
    users: Array<{
      __typename?: 'User';
      id: string;
      firstname: string;
      lastname: string;
      username: string;
      phone: string;
      lastConnection?: number | null | undefined;
      isMe: boolean;
    }>;
  };
};

export type GetUserMessageQueryVariables = Exact<{
  filter: UserMessagesInput;
}>;

export type GetUserMessageQuery = {
  __typename?: 'Query';
  me: {
    __typename?: 'User';
    id: string;
    messages: {
      __typename?: 'DiscussionMessages';
      userId: string;
      count: number;
      user: {
        __typename?: 'User';
        id: string;
        firstname: string;
        lastname: string;
        username: string;
        phone: string;
        lastConnection?: number | null | undefined;
        isMe: boolean;
      };
      messages: Array<{
        __typename?: 'Message';
        id: string;
        text: string;
        createdAt: any;
        isMine: boolean;
        removed: boolean;
        userId?: string | null | undefined;
        authorId: string;
        channelId?: string | null | undefined;
        author: {
          __typename?: 'User';
          id: string;
          firstname: string;
          lastname: string;
          username: string;
          phone: string;
          lastConnection?: number | null | undefined;
          isMe: boolean;
        };
      }>;
    };
  };
};

export type GetAllUserChannelQueryVariables = Exact<{ [key: string]: never }>;

export type GetAllUserChannelQuery = {
  __typename?: 'Query';
  me: {
    __typename?: 'User';
    id: string;
    channels: Array<{
      __typename?: 'Channel';
      id: string;
      name: string;
      authorId: string;
      createdAt: any;
      removed: boolean;
      isAuthor: boolean;
      isPrivate: boolean;
    }>;
  };
};

export type SubscribeToUserLoggedInSubscriptionVariables = Exact<{ [key: string]: never }>;

export type SubscribeToUserLoggedInSubscription = {
  __typename?: 'Subscription';
  userLoggedIn: { __typename?: 'User'; id: string; lastConnection?: number | null | undefined };
};

export type SubscribeToUserLoggedOutSubscriptionVariables = Exact<{ [key: string]: never }>;

export type SubscribeToUserLoggedOutSubscription = {
  __typename?: 'Subscription';
  userLoggedOut: { __typename?: 'User'; id: string; lastConnection?: number | null | undefined };
};

export type SubscribeToUserInfosUpdatedSubscriptionVariables = Exact<{ [key: string]: never }>;

export type SubscribeToUserInfosUpdatedSubscription = {
  __typename?: 'Subscription';
  userInfoUpdated: {
    __typename?: 'User';
    id: string;
    firstname: string;
    lastname: string;
    username: string;
    phone: string;
    lastConnection?: number | null | undefined;
    isMe: boolean;
  };
};

export const ChannelInfosFragmentDoc = gql`
  fragment ChannelInfos on Channel {
    id
    name
    authorId
    createdAt
    removed
    isAuthor
    isPrivate
  }
`;
export const UserBasicInfosFragmentDoc = gql`
  fragment UserBasicInfos on User {
    id
    firstname
    lastname
    username
    phone
    lastConnection
    isMe
  }
`;
export const MessageInfosFragmentDoc = gql`
  fragment MessageInfos on Message {
    id
    text
    createdAt
    isMine
    removed
    userId
    authorId
    channelId
    author {
      ...UserBasicInfos
    }
  }
  ${UserBasicInfosFragmentDoc}
`;
export const ChannelMessageInfosFragmentDoc = gql`
  fragment ChannelMessageInfos on ChannelMessages {
    channelId
    count
    messages {
      ...MessageInfos
    }
  }
  ${MessageInfosFragmentDoc}
`;
export const LoginDocument = gql`
  mutation Login($input: LoginInput!) {
    login(input: $input)
  }
`;
export type LoginMutationFn = Apollo.MutationFunction<LoginMutation, LoginMutationVariables>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useLoginMutation(
  baseOptions?: Apollo.MutationHookOptions<LoginMutation, LoginMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, options);
}
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = Apollo.MutationResult<LoginMutation>;
export type LoginMutationOptions = Apollo.BaseMutationOptions<
  LoginMutation,
  LoginMutationVariables
>;
export const AddChannelDocument = gql`
  mutation AddChannel($input: AddChannelInput!) {
    addChannel(input: $input) {
      ...ChannelInfos
    }
  }
  ${ChannelInfosFragmentDoc}
`;
export type AddChannelMutationFn = Apollo.MutationFunction<
  AddChannelMutation,
  AddChannelMutationVariables
>;

/**
 * __useAddChannelMutation__
 *
 * To run a mutation, you first call `useAddChannelMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddChannelMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addChannelMutation, { data, loading, error }] = useAddChannelMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAddChannelMutation(
  baseOptions?: Apollo.MutationHookOptions<AddChannelMutation, AddChannelMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<AddChannelMutation, AddChannelMutationVariables>(
    AddChannelDocument,
    options
  );
}
export type AddChannelMutationHookResult = ReturnType<typeof useAddChannelMutation>;
export type AddChannelMutationResult = Apollo.MutationResult<AddChannelMutation>;
export type AddChannelMutationOptions = Apollo.BaseMutationOptions<
  AddChannelMutation,
  AddChannelMutationVariables
>;
export const AddChannelParticipantsDocument = gql`
  mutation AddChannelParticipants($input: AddChannelParticipantsInput!) {
    addChannelParticipants(input: $input) {
      ...ChannelInfos
      members {
        ...UserBasicInfos
      }
    }
  }
  ${ChannelInfosFragmentDoc}
  ${UserBasicInfosFragmentDoc}
`;
export type AddChannelParticipantsMutationFn = Apollo.MutationFunction<
  AddChannelParticipantsMutation,
  AddChannelParticipantsMutationVariables
>;

/**
 * __useAddChannelParticipantsMutation__
 *
 * To run a mutation, you first call `useAddChannelParticipantsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddChannelParticipantsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addChannelParticipantsMutation, { data, loading, error }] = useAddChannelParticipantsMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAddChannelParticipantsMutation(
  baseOptions?: Apollo.MutationHookOptions<
    AddChannelParticipantsMutation,
    AddChannelParticipantsMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    AddChannelParticipantsMutation,
    AddChannelParticipantsMutationVariables
  >(AddChannelParticipantsDocument, options);
}
export type AddChannelParticipantsMutationHookResult = ReturnType<
  typeof useAddChannelParticipantsMutation
>;
export type AddChannelParticipantsMutationResult =
  Apollo.MutationResult<AddChannelParticipantsMutation>;
export type AddChannelParticipantsMutationOptions = Apollo.BaseMutationOptions<
  AddChannelParticipantsMutation,
  AddChannelParticipantsMutationVariables
>;
export const RemoveChannelParticipantsDocument = gql`
  mutation RemoveChannelParticipants($input: RemoveChannelParticipantsInput!) {
    removeChannelParticipants(input: $input) {
      ...ChannelInfos
      members {
        ...UserBasicInfos
      }
    }
  }
  ${ChannelInfosFragmentDoc}
  ${UserBasicInfosFragmentDoc}
`;
export type RemoveChannelParticipantsMutationFn = Apollo.MutationFunction<
  RemoveChannelParticipantsMutation,
  RemoveChannelParticipantsMutationVariables
>;

/**
 * __useRemoveChannelParticipantsMutation__
 *
 * To run a mutation, you first call `useRemoveChannelParticipantsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRemoveChannelParticipantsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [removeChannelParticipantsMutation, { data, loading, error }] = useRemoveChannelParticipantsMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useRemoveChannelParticipantsMutation(
  baseOptions?: Apollo.MutationHookOptions<
    RemoveChannelParticipantsMutation,
    RemoveChannelParticipantsMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    RemoveChannelParticipantsMutation,
    RemoveChannelParticipantsMutationVariables
  >(RemoveChannelParticipantsDocument, options);
}
export type RemoveChannelParticipantsMutationHookResult = ReturnType<
  typeof useRemoveChannelParticipantsMutation
>;
export type RemoveChannelParticipantsMutationResult =
  Apollo.MutationResult<RemoveChannelParticipantsMutation>;
export type RemoveChannelParticipantsMutationOptions = Apollo.BaseMutationOptions<
  RemoveChannelParticipantsMutation,
  RemoveChannelParticipantsMutationVariables
>;
export const GetChannelMessagesDocument = gql`
  query GetChannelMessages($channelId: ID!, $filter: ChannelMessagesInput!) {
    channel(channelId: $channelId) {
      ...ChannelInfos
      messages(filter: $filter) {
        ...ChannelMessageInfos
      }
    }
  }
  ${ChannelInfosFragmentDoc}
  ${ChannelMessageInfosFragmentDoc}
`;

/**
 * __useGetChannelMessagesQuery__
 *
 * To run a query within a React component, call `useGetChannelMessagesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetChannelMessagesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetChannelMessagesQuery({
 *   variables: {
 *      channelId: // value for 'channelId'
 *      filter: // value for 'filter'
 *   },
 * });
 */
export function useGetChannelMessagesQuery(
  baseOptions: Apollo.QueryHookOptions<GetChannelMessagesQuery, GetChannelMessagesQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetChannelMessagesQuery, GetChannelMessagesQueryVariables>(
    GetChannelMessagesDocument,
    options
  );
}
export function useGetChannelMessagesLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetChannelMessagesQuery,
    GetChannelMessagesQueryVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetChannelMessagesQuery, GetChannelMessagesQueryVariables>(
    GetChannelMessagesDocument,
    options
  );
}
export type GetChannelMessagesQueryHookResult = ReturnType<typeof useGetChannelMessagesQuery>;
export type GetChannelMessagesLazyQueryHookResult = ReturnType<
  typeof useGetChannelMessagesLazyQuery
>;
export type GetChannelMessagesQueryResult = Apollo.QueryResult<
  GetChannelMessagesQuery,
  GetChannelMessagesQueryVariables
>;
export const GetChannelMembersDocument = gql`
  query GetChannelMembers($channelId: ID!) {
    channel(channelId: $channelId) {
      id
      isPrivate
      isAuthor
      members {
        ...UserBasicInfos
      }
    }
  }
  ${UserBasicInfosFragmentDoc}
`;

/**
 * __useGetChannelMembersQuery__
 *
 * To run a query within a React component, call `useGetChannelMembersQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetChannelMembersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetChannelMembersQuery({
 *   variables: {
 *      channelId: // value for 'channelId'
 *   },
 * });
 */
export function useGetChannelMembersQuery(
  baseOptions: Apollo.QueryHookOptions<GetChannelMembersQuery, GetChannelMembersQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetChannelMembersQuery, GetChannelMembersQueryVariables>(
    GetChannelMembersDocument,
    options
  );
}
export function useGetChannelMembersLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<GetChannelMembersQuery, GetChannelMembersQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetChannelMembersQuery, GetChannelMembersQueryVariables>(
    GetChannelMembersDocument,
    options
  );
}
export type GetChannelMembersQueryHookResult = ReturnType<typeof useGetChannelMembersQuery>;
export type GetChannelMembersLazyQueryHookResult = ReturnType<typeof useGetChannelMembersLazyQuery>;
export type GetChannelMembersQueryResult = Apollo.QueryResult<
  GetChannelMembersQuery,
  GetChannelMembersQueryVariables
>;
export const SubscribeToChannelAddedDocument = gql`
  subscription SubscribeToChannelAdded {
    channelAdded {
      ...ChannelInfos
    }
  }
  ${ChannelInfosFragmentDoc}
`;

/**
 * __useSubscribeToChannelAddedSubscription__
 *
 * To run a query within a React component, call `useSubscribeToChannelAddedSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeToChannelAddedSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeToChannelAddedSubscription({
 *   variables: {
 *   },
 * });
 */
export function useSubscribeToChannelAddedSubscription(
  baseOptions?: Apollo.SubscriptionHookOptions<
    SubscribeToChannelAddedSubscription,
    SubscribeToChannelAddedSubscriptionVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSubscription<
    SubscribeToChannelAddedSubscription,
    SubscribeToChannelAddedSubscriptionVariables
  >(SubscribeToChannelAddedDocument, options);
}
export type SubscribeToChannelAddedSubscriptionHookResult = ReturnType<
  typeof useSubscribeToChannelAddedSubscription
>;
export type SubscribeToChannelAddedSubscriptionResult =
  Apollo.SubscriptionResult<SubscribeToChannelAddedSubscription>;
export const SubscribeToChannelMembersUpdatedDocument = gql`
  subscription SubscribeToChannelMembersUpdated {
    userChannelMemberUpdated {
      ...ChannelInfos
      members {
        ...UserBasicInfos
      }
    }
  }
  ${ChannelInfosFragmentDoc}
  ${UserBasicInfosFragmentDoc}
`;

/**
 * __useSubscribeToChannelMembersUpdatedSubscription__
 *
 * To run a query within a React component, call `useSubscribeToChannelMembersUpdatedSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeToChannelMembersUpdatedSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeToChannelMembersUpdatedSubscription({
 *   variables: {
 *   },
 * });
 */
export function useSubscribeToChannelMembersUpdatedSubscription(
  baseOptions?: Apollo.SubscriptionHookOptions<
    SubscribeToChannelMembersUpdatedSubscription,
    SubscribeToChannelMembersUpdatedSubscriptionVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSubscription<
    SubscribeToChannelMembersUpdatedSubscription,
    SubscribeToChannelMembersUpdatedSubscriptionVariables
  >(SubscribeToChannelMembersUpdatedDocument, options);
}
export type SubscribeToChannelMembersUpdatedSubscriptionHookResult = ReturnType<
  typeof useSubscribeToChannelMembersUpdatedSubscription
>;
export type SubscribeToChannelMembersUpdatedSubscriptionResult =
  Apollo.SubscriptionResult<SubscribeToChannelMembersUpdatedSubscription>;
export const AddDiscussionMessageDocument = gql`
  mutation AddDiscussionMessage($input: AddDiscussionMessageInput!) {
    addDiscussionMessage(input: $input) {
      ...MessageInfos
    }
  }
  ${MessageInfosFragmentDoc}
`;
export type AddDiscussionMessageMutationFn = Apollo.MutationFunction<
  AddDiscussionMessageMutation,
  AddDiscussionMessageMutationVariables
>;

/**
 * __useAddDiscussionMessageMutation__
 *
 * To run a mutation, you first call `useAddDiscussionMessageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddDiscussionMessageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addDiscussionMessageMutation, { data, loading, error }] = useAddDiscussionMessageMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAddDiscussionMessageMutation(
  baseOptions?: Apollo.MutationHookOptions<
    AddDiscussionMessageMutation,
    AddDiscussionMessageMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<AddDiscussionMessageMutation, AddDiscussionMessageMutationVariables>(
    AddDiscussionMessageDocument,
    options
  );
}
export type AddDiscussionMessageMutationHookResult = ReturnType<
  typeof useAddDiscussionMessageMutation
>;
export type AddDiscussionMessageMutationResult =
  Apollo.MutationResult<AddDiscussionMessageMutation>;
export type AddDiscussionMessageMutationOptions = Apollo.BaseMutationOptions<
  AddDiscussionMessageMutation,
  AddDiscussionMessageMutationVariables
>;
export const AddChannelMessageDocument = gql`
  mutation AddChannelMessage($input: AddChannelMessageInput!) {
    addChannelMessage(input: $input) {
      ...MessageInfos
    }
  }
  ${MessageInfosFragmentDoc}
`;
export type AddChannelMessageMutationFn = Apollo.MutationFunction<
  AddChannelMessageMutation,
  AddChannelMessageMutationVariables
>;

/**
 * __useAddChannelMessageMutation__
 *
 * To run a mutation, you first call `useAddChannelMessageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddChannelMessageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addChannelMessageMutation, { data, loading, error }] = useAddChannelMessageMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAddChannelMessageMutation(
  baseOptions?: Apollo.MutationHookOptions<
    AddChannelMessageMutation,
    AddChannelMessageMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<AddChannelMessageMutation, AddChannelMessageMutationVariables>(
    AddChannelMessageDocument,
    options
  );
}
export type AddChannelMessageMutationHookResult = ReturnType<typeof useAddChannelMessageMutation>;
export type AddChannelMessageMutationResult = Apollo.MutationResult<AddChannelMessageMutation>;
export type AddChannelMessageMutationOptions = Apollo.BaseMutationOptions<
  AddChannelMessageMutation,
  AddChannelMessageMutationVariables
>;
export const SubscribeToDiscussionMessageDocument = gql`
  subscription SubscribeToDiscussionMessage {
    messageAdded {
      ...MessageInfos
    }
  }
  ${MessageInfosFragmentDoc}
`;

/**
 * __useSubscribeToDiscussionMessageSubscription__
 *
 * To run a query within a React component, call `useSubscribeToDiscussionMessageSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeToDiscussionMessageSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeToDiscussionMessageSubscription({
 *   variables: {
 *   },
 * });
 */
export function useSubscribeToDiscussionMessageSubscription(
  baseOptions?: Apollo.SubscriptionHookOptions<
    SubscribeToDiscussionMessageSubscription,
    SubscribeToDiscussionMessageSubscriptionVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSubscription<
    SubscribeToDiscussionMessageSubscription,
    SubscribeToDiscussionMessageSubscriptionVariables
  >(SubscribeToDiscussionMessageDocument, options);
}
export type SubscribeToDiscussionMessageSubscriptionHookResult = ReturnType<
  typeof useSubscribeToDiscussionMessageSubscription
>;
export type SubscribeToDiscussionMessageSubscriptionResult =
  Apollo.SubscriptionResult<SubscribeToDiscussionMessageSubscription>;
export const UpdateUserDocument = gql`
  mutation UpdateUser($input: UpdateUserInput!) {
    updateUser(input: $input) {
      ...UserBasicInfos
    }
  }
  ${UserBasicInfosFragmentDoc}
`;
export type UpdateUserMutationFn = Apollo.MutationFunction<
  UpdateUserMutation,
  UpdateUserMutationVariables
>;

/**
 * __useUpdateUserMutation__
 *
 * To run a mutation, you first call `useUpdateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUserMutation, { data, loading, error }] = useUpdateUserMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateUserMutation(
  baseOptions?: Apollo.MutationHookOptions<UpdateUserMutation, UpdateUserMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<UpdateUserMutation, UpdateUserMutationVariables>(
    UpdateUserDocument,
    options
  );
}
export type UpdateUserMutationHookResult = ReturnType<typeof useUpdateUserMutation>;
export type UpdateUserMutationResult = Apollo.MutationResult<UpdateUserMutation>;
export type UpdateUserMutationOptions = Apollo.BaseMutationOptions<
  UpdateUserMutation,
  UpdateUserMutationVariables
>;
export const MeDocument = gql`
  query Me {
    me {
      ...UserBasicInfos
    }
  }
  ${UserBasicInfosFragmentDoc}
`;

/**
 * __useMeQuery__
 *
 * To run a query within a React component, call `useMeQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMeQuery({
 *   variables: {
 *   },
 * });
 */
export function useMeQuery(baseOptions?: Apollo.QueryHookOptions<MeQuery, MeQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<MeQuery, MeQueryVariables>(MeDocument, options);
}
export function useMeLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<MeQuery, MeQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<MeQuery, MeQueryVariables>(MeDocument, options);
}
export type MeQueryHookResult = ReturnType<typeof useMeQuery>;
export type MeLazyQueryHookResult = ReturnType<typeof useMeLazyQuery>;
export type MeQueryResult = Apollo.QueryResult<MeQuery, MeQueryVariables>;
export const GetAllUsersDocument = gql`
  query GetAllUsers {
    me {
      id
      users {
        ...UserBasicInfos
      }
    }
  }
  ${UserBasicInfosFragmentDoc}
`;

/**
 * __useGetAllUsersQuery__
 *
 * To run a query within a React component, call `useGetAllUsersQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAllUsersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAllUsersQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetAllUsersQuery(
  baseOptions?: Apollo.QueryHookOptions<GetAllUsersQuery, GetAllUsersQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetAllUsersQuery, GetAllUsersQueryVariables>(GetAllUsersDocument, options);
}
export function useGetAllUsersLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<GetAllUsersQuery, GetAllUsersQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetAllUsersQuery, GetAllUsersQueryVariables>(
    GetAllUsersDocument,
    options
  );
}
export type GetAllUsersQueryHookResult = ReturnType<typeof useGetAllUsersQuery>;
export type GetAllUsersLazyQueryHookResult = ReturnType<typeof useGetAllUsersLazyQuery>;
export type GetAllUsersQueryResult = Apollo.QueryResult<
  GetAllUsersQuery,
  GetAllUsersQueryVariables
>;
export const GetUserMessageDocument = gql`
  query GetUserMessage($filter: UserMessagesInput!) {
    me {
      id
      messages(filter: $filter) {
        userId
        user {
          ...UserBasicInfos
        }
        count
        messages {
          ...MessageInfos
        }
      }
    }
  }
  ${UserBasicInfosFragmentDoc}
  ${MessageInfosFragmentDoc}
`;

/**
 * __useGetUserMessageQuery__
 *
 * To run a query within a React component, call `useGetUserMessageQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserMessageQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUserMessageQuery({
 *   variables: {
 *      filter: // value for 'filter'
 *   },
 * });
 */
export function useGetUserMessageQuery(
  baseOptions: Apollo.QueryHookOptions<GetUserMessageQuery, GetUserMessageQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetUserMessageQuery, GetUserMessageQueryVariables>(
    GetUserMessageDocument,
    options
  );
}
export function useGetUserMessageLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<GetUserMessageQuery, GetUserMessageQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetUserMessageQuery, GetUserMessageQueryVariables>(
    GetUserMessageDocument,
    options
  );
}
export type GetUserMessageQueryHookResult = ReturnType<typeof useGetUserMessageQuery>;
export type GetUserMessageLazyQueryHookResult = ReturnType<typeof useGetUserMessageLazyQuery>;
export type GetUserMessageQueryResult = Apollo.QueryResult<
  GetUserMessageQuery,
  GetUserMessageQueryVariables
>;
export const GetAllUserChannelDocument = gql`
  query GetAllUserChannel {
    me {
      id
      channels {
        ...ChannelInfos
      }
    }
  }
  ${ChannelInfosFragmentDoc}
`;

/**
 * __useGetAllUserChannelQuery__
 *
 * To run a query within a React component, call `useGetAllUserChannelQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAllUserChannelQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAllUserChannelQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetAllUserChannelQuery(
  baseOptions?: Apollo.QueryHookOptions<GetAllUserChannelQuery, GetAllUserChannelQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetAllUserChannelQuery, GetAllUserChannelQueryVariables>(
    GetAllUserChannelDocument,
    options
  );
}
export function useGetAllUserChannelLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<GetAllUserChannelQuery, GetAllUserChannelQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetAllUserChannelQuery, GetAllUserChannelQueryVariables>(
    GetAllUserChannelDocument,
    options
  );
}
export type GetAllUserChannelQueryHookResult = ReturnType<typeof useGetAllUserChannelQuery>;
export type GetAllUserChannelLazyQueryHookResult = ReturnType<typeof useGetAllUserChannelLazyQuery>;
export type GetAllUserChannelQueryResult = Apollo.QueryResult<
  GetAllUserChannelQuery,
  GetAllUserChannelQueryVariables
>;
export const SubscribeToUserLoggedInDocument = gql`
  subscription SubscribeToUserLoggedIn {
    userLoggedIn {
      id
      lastConnection
    }
  }
`;

/**
 * __useSubscribeToUserLoggedInSubscription__
 *
 * To run a query within a React component, call `useSubscribeToUserLoggedInSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeToUserLoggedInSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeToUserLoggedInSubscription({
 *   variables: {
 *   },
 * });
 */
export function useSubscribeToUserLoggedInSubscription(
  baseOptions?: Apollo.SubscriptionHookOptions<
    SubscribeToUserLoggedInSubscription,
    SubscribeToUserLoggedInSubscriptionVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSubscription<
    SubscribeToUserLoggedInSubscription,
    SubscribeToUserLoggedInSubscriptionVariables
  >(SubscribeToUserLoggedInDocument, options);
}
export type SubscribeToUserLoggedInSubscriptionHookResult = ReturnType<
  typeof useSubscribeToUserLoggedInSubscription
>;
export type SubscribeToUserLoggedInSubscriptionResult =
  Apollo.SubscriptionResult<SubscribeToUserLoggedInSubscription>;
export const SubscribeToUserLoggedOutDocument = gql`
  subscription SubscribeToUserLoggedOut {
    userLoggedOut {
      id
      lastConnection
    }
  }
`;

/**
 * __useSubscribeToUserLoggedOutSubscription__
 *
 * To run a query within a React component, call `useSubscribeToUserLoggedOutSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeToUserLoggedOutSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeToUserLoggedOutSubscription({
 *   variables: {
 *   },
 * });
 */
export function useSubscribeToUserLoggedOutSubscription(
  baseOptions?: Apollo.SubscriptionHookOptions<
    SubscribeToUserLoggedOutSubscription,
    SubscribeToUserLoggedOutSubscriptionVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSubscription<
    SubscribeToUserLoggedOutSubscription,
    SubscribeToUserLoggedOutSubscriptionVariables
  >(SubscribeToUserLoggedOutDocument, options);
}
export type SubscribeToUserLoggedOutSubscriptionHookResult = ReturnType<
  typeof useSubscribeToUserLoggedOutSubscription
>;
export type SubscribeToUserLoggedOutSubscriptionResult =
  Apollo.SubscriptionResult<SubscribeToUserLoggedOutSubscription>;
export const SubscribeToUserInfosUpdatedDocument = gql`
  subscription SubscribeToUserInfosUpdated {
    userInfoUpdated {
      ...UserBasicInfos
    }
  }
  ${UserBasicInfosFragmentDoc}
`;

/**
 * __useSubscribeToUserInfosUpdatedSubscription__
 *
 * To run a query within a React component, call `useSubscribeToUserInfosUpdatedSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeToUserInfosUpdatedSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeToUserInfosUpdatedSubscription({
 *   variables: {
 *   },
 * });
 */
export function useSubscribeToUserInfosUpdatedSubscription(
  baseOptions?: Apollo.SubscriptionHookOptions<
    SubscribeToUserInfosUpdatedSubscription,
    SubscribeToUserInfosUpdatedSubscriptionVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSubscription<
    SubscribeToUserInfosUpdatedSubscription,
    SubscribeToUserInfosUpdatedSubscriptionVariables
  >(SubscribeToUserInfosUpdatedDocument, options);
}
export type SubscribeToUserInfosUpdatedSubscriptionHookResult = ReturnType<
  typeof useSubscribeToUserInfosUpdatedSubscription
>;
export type SubscribeToUserInfosUpdatedSubscriptionResult =
  Apollo.SubscriptionResult<SubscribeToUserInfosUpdatedSubscription>;

export interface PossibleTypesResultData {
  possibleTypes: {
    [key: string]: string[];
  };
}
const result: PossibleTypesResultData = {
  possibleTypes: {
    Destination: ['Channel', 'User'],
  },
};
export default result;
