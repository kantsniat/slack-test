import { TextField } from '@material-ui/core';
import { FC, KeyboardEvent, useState } from 'react';
import { CustomButton } from '../../../common/CustomButton';
import { useLoginMutation } from '../../../generated/graphql';
import authService from '../../../services/authServices';
import ErrorMessage from '../../../services/ErrorMessage';
import useStyle from './styles';

const SlackLoginForm: FC = () => {
  const classes = useStyle();

  const [username, setUsername] = useState<string | null>('');
  const [password, setPassword] = useState<string | null>('');
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const [errorLog, setErrorLog] = useState('');

  const [loginMutation, { loading: isLoading }] = useLoginMutation();

  const handleLogin = () => {
    if (!isLoading && username?.trim() && password?.length) {
      setErrorLog('');
      loginMutation({
        variables: {
          input: {
            password,
            username: username.trim(),
          },
        },
      })
        .then(resp => {
          if (resp.data?.login) {
            authService.setAccessToken(resp.data.login);
            setIsLoggedIn(true);
          }
        })
        .catch(error => {
          if (error.networkError) {
            setErrorLog('Network error');
          } else if (error.graphQLErrors.length) {
            const firstError = error.graphQLErrors[0].message;
            if (firstError === ErrorMessage.LOGIN_OR_PASSWORD_INCORRECT) {
              setErrorLog('Incorrect login or password');
            } else {
              setErrorLog('Internal server error');
            }
          }
        });
    }
  };

  const handleEnterPress = (evt: KeyboardEvent<HTMLDivElement>) => {
    if (evt.key === 'Enter') {
      handleLogin();
    }
  };

  if (isLoggedIn) {
    window.location.href = '/';
    return null;
  }

  return (
    <div className={classes.containerSlackLoginForm} onKeyDown={handleEnterPress}>
      <form onSubmit={handleLogin}>
        <div>
          <TextField
            onChange={evt => setUsername(evt.target.value || null)}
            value={username || ''}
            error={username === undefined}
            className={classes.inputText}
            placeholder="Login"
          />
          <TextField
            onChange={evt => setPassword(evt.target.value || null)}
            value={password || ''}
            error={password === undefined}
            className={classes.inputText}
            type="password"
            placeholder="Password"
            autoComplete=""
          />
        </div>
        <h5 className={classes.errorMessage}>{errorLog}</h5>
        <CustomButton
          onClick={handleLogin}
          className={classes.buttonSubmit}
          disabled={!password || !username}
          isLoading={isLoading}
        >
          Connexion
        </CustomButton>
      </form>
    </div>
  );
};

export default SlackLoginForm;
