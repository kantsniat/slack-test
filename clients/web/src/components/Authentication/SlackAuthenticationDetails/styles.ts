import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(theme => ({
  containerSlackAuthenticationDetails: {},
  detailText: {
    fontSize: 22,
    fontWeight: 500,
    color: theme.colors.white,
    marginTop: 90,
    textAlign: 'justify',
  },
  rightContainer: {
    padding: '148px 50px 70px 50px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    position: 'relative',
    [theme.breakpoints.down('sm')]: {
      padding: '148px 50px 70px 50px',
    },
  },
  backgroundOverlay: {
    backgroundColor: theme.colors.valentino,
    opacity: 1,
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  title: {
    marginTop: 45,
  },
}));
