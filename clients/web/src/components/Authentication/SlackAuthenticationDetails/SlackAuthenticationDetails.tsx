import { Grid, Typography } from '@material-ui/core';
import classnames from 'classnames';
import { FC } from 'react';
import useStyle from './styles';

interface SlackAuthenticationDetailsProps {
  className: string;
}

const SlackAuthenticationDetails: FC<SlackAuthenticationDetailsProps> = props => {
  const { className } = props;
  const classes = useStyle();
  return (
    <Grid
      item={true}
      sm={5}
      xs={12}
      className={classnames(className, classes.containerSlackAuthenticationDetails)}
    >
      <div className={classes.backgroundOverlay} />
      <div className={classes.rightContainer}>
        <Typography color="textSecondary" variant="h4">
          Welcome to
        </Typography>
        <Typography className={classes.title} color="textSecondary" variant="h3">
          Slack clone
        </Typography>
        <Typography variant="body2" className={classes.detailText}>
          All login & password are in the readme.md file of the project repository.
        </Typography>
      </div>
    </Grid>
  );
};

export default SlackAuthenticationDetails;
