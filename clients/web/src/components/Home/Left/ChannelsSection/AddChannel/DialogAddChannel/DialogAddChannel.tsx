import { FormControlLabel, Switch } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { FC, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useAddChannelMutation } from '../../../../../../generated/graphql';
import { updateCacheAfterAddingChannel } from '../../../../../../updateCache/message/addChannel';

interface DialogAddChannelProps {
  isOpen: boolean;
  close: () => void;
}

const DialogAddChannel: FC<DialogAddChannelProps> = props => {
  const { isOpen: open, close } = props;

  const [name, setName] = useState('');
  const [isPrivate, setIsPrivate] = useState(false);

  useEffect(() => {
    if (open) {
      setName('');
      setIsPrivate(false);
    }
  }, [open]);

  const [addChannelMutation, { loading }] = useAddChannelMutation({
    update: updateCacheAfterAddingChannel,
  });

  const history = useHistory();

  const onSubmit = () => {
    if (!loading && name.trim()) {
      addChannelMutation({
        variables: {
          input: {
            isPublic: !isPrivate,
            name: name.trim(),
          },
        },
      }).then(({ data }) => {
        if (data?.addChannel) {
          history.push(`/channel/${data.addChannel.id}`);
          close();
        }
      });
    }
  };

  return (
    <div>
      <Dialog open={open} onClose={close} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Add new channel</DialogTitle>
        <DialogContent>
          <DialogContentText>To Add new channel, please enter the name here.</DialogContentText>
          <TextField
            autoFocus={true}
            margin="dense"
            id="name"
            value={name}
            onChange={evt => setName(evt.target.value)}
            label="Name"
            type="text"
            fullWidth={true}
          />
          <FormControlLabel
            control={
              <Switch checked={isPrivate} onChange={(evt, checked) => setIsPrivate(checked)} />
            }
            label="Private"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={close} color="primary">
            Cancel
          </Button>
          <Button disabled={!name.trim()} onClick={onSubmit} color="primary">
            Add
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default DialogAddChannel;
