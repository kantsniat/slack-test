import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(() => ({
  containerAddChannel: {
    marginLeft: 24,
    textTransform: 'none',
  },
}));
