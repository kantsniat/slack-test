import { Add } from '@material-ui/icons';
import { FC, useState } from 'react';
import { CustomButton } from '../../../../../common/CustomButton';
import { DialogAddChannel } from './DialogAddChannel';
import useStyle from './styles';

const AddChannel: FC = () => {
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const classes = useStyle();

  return (
    <>
      <CustomButton onClick={() => setIsDialogOpen(true)} className={classes.containerAddChannel}>
        <Add /> Add new
      </CustomButton>
      <DialogAddChannel isOpen={isDialogOpen} close={() => setIsDialogOpen(false)} />
    </>
  );
};

export default AddChannel;
