import { Theme } from '@material-ui/core';
import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles((theme: Theme) => ({
  containerLeft: {
    flex: 1,
    maxWidth: 260,
    width: 260,
    backgroundColor: theme.colors.valentino,
  },
  centerSection: {
    maxHeight: 'calc(100vh - 82px)',
    overflowY: 'auto',
  },
}));
