import { Typography } from '@material-ui/core';
import { FC } from 'react';
import { useMeQuery } from '../../../../generated/graphql';
import useStyle from './styles';

const UserSection: FC = () => {
  const classes = useStyle();
  const { data } = useMeQuery();
  const user = data?.me;
  if (!user) {
    return null;
  }
  const { firstname, username } = user;
  return (
    <div className={classes.containerUserSection}>
      <Typography color="textSecondary" variant="h6">
        {firstname}{' '}
        <Typography color="textSecondary" variant="caption">
          (@{username})
        </Typography>
      </Typography>
    </div>
  );
};

export default UserSection;
