import { Theme } from '@material-ui/core';
import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles<Theme>(() => ({
  containerUserSection: {
    textAlign: 'center',
    marginTop: 7,
  },
}));
