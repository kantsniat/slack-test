import { List, Typography } from '@material-ui/core';
import { FC } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { DirectMessageMenuItem } from '../../../../common/DirectMessageMenuItem';
import { Skeleton } from '../../../../common/Skeleton';
import { useGetAllUsersQuery } from '../../../../generated/graphql';
import { isChannelFn } from '../../../../utils/locationUtils';
import { getUserName } from '../../../../utils/userUtils';
import useStyle from './styles';

const DirectMessagesSection: FC = () => {
  const classes = useStyle();

  const { id } = useParams<{ id: string }>();
  const { pathname } = useLocation();

  const isChannel = isChannelFn(pathname);

  const { data, error, loading } = useGetAllUsersQuery();
  const users = data?.me.users || [];

  const usersWithoutMe = users.filter(({ isMe }) => !isMe);
  return (
    <div className={classes.containerDirectMessagesSection}>
      <Typography className={classes.title} color="textSecondary">
        Direct messages
      </Typography>
      <List>
        {error && <Typography color="error">Error</Typography>}
        {loading && (
          <>
            <Skeleton height={35} />
            <Skeleton height={35} />
            <Skeleton height={35} />
          </>
        )}
        {!loading &&
          usersWithoutMe.map(user => (
            <DirectMessageMenuItem
              isActive={!isChannel && +user.id === +id}
              userName={getUserName(user)}
              userId={+user.id}
              key={user.id}
              isOnline={user.lastConnection === 0}
            />
          ))}
      </List>
    </div>
  );
};

export default DirectMessagesSection;
