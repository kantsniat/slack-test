import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { FC, useEffect, useState } from 'react';
import { Avatar } from '../../../../common/Avatar';
import { CustomButton } from '../../../../common/CustomButton';
import { globalSnackbarVar } from '../../../../config/reactiveVars';
import { useMeQuery, useUpdateUserMutation } from '../../../../generated/graphql';
import { getUserName } from '../../../../utils/userUtils';

interface UserInfosProps {
  isOpen: boolean;
  close: () => void;
}

const UserInfos: FC<UserInfosProps> = props => {
  const { isOpen, close } = props;

  const { data } = useMeQuery();

  const user = data?.me;

  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [phone, setPhone] = useState('');

  const [mutationUpdateUser, { loading }] = useUpdateUserMutation();

  useEffect(() => {
    if (user) {
      setFirstname(user.firstname);
      setLastname(user.lastname);
      setPhone(user.phone);
    }
  }, [user]);

  if (!user) {
    return null;
  }

  const onUpdate = () => {
    mutationUpdateUser({
      variables: {
        input: {
          firstname: firstname.trim(),
          lastname: lastname.trim(),
          phone: phone.trim(),
        },
      },
    })
      .then(() => {
        globalSnackbarVar({
          open: true,
          message: 'Success',
          type: 'SUCCESS',
        });
        close();
      })
      .catch(() => {
        globalSnackbarVar({
          open: true,
          message: 'An error occured',
          type: 'ERROR',
        });
      });
  };

  const editEnabled =
    firstname !== user.firstname || lastname !== user.lastname || phone !== user.phone;

  return (
    <div>
      <Dialog open={isOpen} onClose={close} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Subscribe</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To subscribe to this website, please enter your email address here. We will send updates
            occasionally.
            <Avatar userName={getUserName(user)} indicatorStatus="isMine" />
          </DialogContentText>
          <TextField
            onChange={evt => setFirstname(evt.target.value)}
            margin="dense"
            value={firstname}
            label="Firstname"
            fullWidth={true}
            error={!firstname.trim()}
          />
          <TextField
            onChange={evt => setLastname(evt.target.value)}
            margin="dense"
            value={lastname}
            label="LastName"
            fullWidth={true}
            error={!lastname.trim()}
          />
          <TextField
            onChange={evt => setPhone(evt.target.value)}
            margin="dense"
            value={phone}
            label="Phone"
            fullWidth={true}
            error={!phone.trim()}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={close} color="primary">
            Cancel
          </Button>
          <CustomButton disabled={!editEnabled || loading} onClick={onUpdate} color="primary">
            Enregistrer
          </CustomButton>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default UserInfos;
