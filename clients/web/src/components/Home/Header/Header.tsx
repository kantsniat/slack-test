import { Typography } from '@material-ui/core';
import { FC } from 'react';
import useStyle from './styles';
import { UserAvatar } from './userAvatar';

interface HeaderProps {
  userFullName: string;
}

const Header: FC<HeaderProps> = props => {
  const { userFullName } = props;
  const classes = useStyle();

  return (
    <div className={classes.containerHeader}>
      <Typography variant="h5" className={classes.title}>
        Slack clone
      </Typography>
      <UserAvatar userFullName={userFullName} />
    </div>
  );
};

export default Header;
