import { Button, List, ListItem, Popover } from '@material-ui/core';
import { Person, TimeToLeave } from '@material-ui/icons';
import { FC, useRef, useState } from 'react';
import { Avatar } from '../../../../common/Avatar';
import authService from '../../../../services/authServices';
import UserInfos from '../UserInfos/UserInfos';
import useStyle from './styles';

interface UserAvatarProps {
  userFullName: string;
}

const UserAvatar: FC<UserAvatarProps> = props => {
  const { userFullName } = props;
  const classes = useStyle();
  const [isOpen, setIsOpen] = useState(false);
  const [isUserInfosOpen, setIsUserInfosOpen] = useState(false);

  const buttonRef = useRef<HTMLButtonElement>(null);

  return (
    <>
      <Button
        className={classes.containerUserAvatar}
        ref={buttonRef}
        onClick={() => setIsOpen(true)}
      >
        <Avatar className={classes.avatar} userName={userFullName} indicatorStatus="isMine" />
      </Button>
      <Popover
        open={isOpen}
        anchorEl={buttonRef.current}
        onClose={() => setIsOpen(false)}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        classes={{
          paper: classes.popover,
        }}
      >
        <List>
          <ListItem
            className={classes.listItem}
            button={true}
            onClick={() => {
              setIsOpen(false);
              setIsUserInfosOpen(true);
            }}
          >
            <Person /> Profile
          </ListItem>
          <ListItem
            className={classes.listItem}
            button={true}
            onClick={e => {
              e.preventDefault();
              authService.clear();
              window.location.href = '/login';
            }}
          >
            <TimeToLeave />
            Logout
          </ListItem>
        </List>
      </Popover>
      <UserInfos isOpen={isUserInfosOpen} close={() => setIsUserInfosOpen(false)} />
    </>
  );
};

export default UserAvatar;
