import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(theme => ({
  containerUserAvatar: {
    marginLeft: 'auto',
    marginRight: 7,
  },
  avatar: {},
  popover: {
    ...theme.containerStyles.flexRow,
  },
  listItem: {
    minWidth: 200,
  },
}));
