import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(() => ({
  containerChannelMessageLoader: {
    flex: 1,
  },
}));
