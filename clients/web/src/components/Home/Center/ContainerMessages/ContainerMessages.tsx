import { Typography } from '@material-ui/core';
import { FC } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { isChannelFn } from '../../../../utils/locationUtils';
import { ChannelMessageLoader } from './ChannelMessageLoader';
import { DiscussionMessageLoader } from './DiscussionMessageLoader';
import useStyle from './styles';

const ContainerMessages: FC = () => {
  const classes = useStyle();
  const { pathname } = useLocation();
  const { id } = useParams<{ id: string }>();

  const isChannel = isChannelFn(pathname);

  return (
    <div className={classes.containerMessages}>
      {isChannel && id && <ChannelMessageLoader channelId={+id} />}
      {!isChannel && id && <DiscussionMessageLoader userId={+id} />}
      {!id && (
        <div className={classes.emptyText}>
          <Typography>Select one item from the left</Typography>
        </div>
      )}
    </div>
  );
};

export default ContainerMessages;
