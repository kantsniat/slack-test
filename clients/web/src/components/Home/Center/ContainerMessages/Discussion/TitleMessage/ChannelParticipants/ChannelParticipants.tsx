import { IconButton, Typography } from '@material-ui/core';
import { Edit } from '@material-ui/icons';
import { FC, useState } from 'react';
import Avatar from '../../../../../../../common/Avatar/Avatar';
import { Loading } from '../../../../../../../common/Loading';
import ModalAddParticipants from '../../../../../../../common/ModalAddParticipants/ModalAddparticipants';
import {
  useGetAllUsersQuery,
  useGetChannelMembersQuery,
} from '../../../../../../../generated/graphql';
import { getUserName } from '../../../../../../../utils/userUtils';
import useStyle from './styles';

interface ChannelParticipantsProps {
  channelId: number;
}

const ChannelParticipants: FC<ChannelParticipantsProps> = props => {
  const { channelId } = props;
  const [isOpen, setIsOpen] = useState(false);
  const classes = useStyle();
  const { data, loading } = useGetChannelMembersQuery({
    variables: { channelId: channelId.toString() },
  });

  const { data: userData, loading: userLoading } = useGetAllUsersQuery();

  if (loading || userLoading) {
    return (
      <div className={classes.containerChannelParticipants}>
        <Loading size={25} />
      </div>
    );
  }

  const allUsers = userData?.me?.users || [];

  const users = data?.channel?.members || [];

  const isAuthor = data?.channel?.isAuthor;

  const isPrivate = data?.channel?.isPrivate;

  if (!isPrivate) {
    return (
      <div className={classes.containerChannelParticipants}>
        <Typography color="error">Public channel</Typography>
      </div>
    );
  }

  return (
    <div className={classes.containerChannelParticipants}>
      {users.map(user => (
        <Avatar
          userName={getUserName(user)}
          indicatorStatus={user.isMe ? 'isMine' : user.lastConnection === 0 ? 'online' : 'offline'}
          isCircle={true}
        />
      ))}
      {Boolean(isAuthor) && (
        <>
          <IconButton onClick={() => setIsOpen(true)}>
            <Edit />
          </IconButton>
          <ModalAddParticipants
            channelId={channelId.toString()}
            memberUsers={users}
            users={allUsers}
            isOpen={isOpen}
            onClose={() => setIsOpen(false)}
          />
        </>
      )}
    </div>
  );
};

export default ChannelParticipants;
