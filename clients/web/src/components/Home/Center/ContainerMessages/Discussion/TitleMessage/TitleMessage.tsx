import { Divider, Typography } from '@material-ui/core';
import { FC } from 'react';
import { useParams } from 'react-router-dom';
import { Avatar } from '../../../../../../common/Avatar';
import { ChannelParticipants } from './ChannelParticipants';
import useStyle from './styles';

interface TitleMessageProps {
  title: string;
  isChannel: boolean;
  isOnline: boolean;
}

const TitleMessage: FC<TitleMessageProps> = props => {
  const { title, isChannel, isOnline } = props;
  const classes = useStyle();
  const { id } = useParams<{ id: string }>();
  return (
    <div className={classes.containerTitleMessage}>
      <div className={classes.titleContainer}>
        {!isChannel && (
          <Avatar indicatorStatus={isOnline ? 'online' : 'offline'} size={35} userName="name" />
        )}
        <Typography variant="h5">
          {isChannel && '#'}
          <span className={classes.title}>{title}</span>
        </Typography>
        {isChannel && <ChannelParticipants channelId={+id} />}
      </div>
      <Divider className={classes.divider} />
    </div>
  );
};

export default TitleMessage;
