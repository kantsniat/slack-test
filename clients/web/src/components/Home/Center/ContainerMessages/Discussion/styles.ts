import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(theme => ({
  containerDiscussion: {
    height: 'calc(100vh - 48px)',
    ...theme.containerStyles.flexColumn,
  },
}));
