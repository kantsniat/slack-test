import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(theme => ({
  containerMessages: {
    flex: 1,
  },
  emptyText: {
    ...theme.containerStyles.flexColumn,
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
}));
