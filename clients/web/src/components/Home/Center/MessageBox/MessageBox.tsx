import { FC, KeyboardEvent, useState } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { CustomInput } from '../../../../common/CustomInput';
import { globalSnackbarVar } from '../../../../config/reactiveVars';
import {
  useAddChannelMessageMutation,
  useAddDiscussionMessageMutation,
} from '../../../../generated/graphql';
import { updateCacheAfterAddingChannelMessage } from '../../../../updateCache/message/addChannelMessage';
import { updateCacheAfterAddingDiscussionMessage } from '../../../../updateCache/message/addMessage';
import { isChannelFn } from '../../../../utils/locationUtils';
import useStyle from './styles';

const MessageBox: FC = () => {
  const [message, setMessage] = useState('');
  const classes = useStyle();

  const { id } = useParams<{ id: string }>();
  const { pathname } = useLocation();
  const isChannel = isChannelFn(pathname);
  const [mutationAddMessage, { loading }] = useAddDiscussionMessageMutation({
    update: updateCacheAfterAddingDiscussionMessage,
  });

  const [mutationAddChannelMessage, { loading: channelMessageLoading }] =
    useAddChannelMessageMutation({
      update: updateCacheAfterAddingChannelMessage,
    });

  const onSubmit = () => {
    if (message.trim().length) {
      if (!isChannel && !loading) {
        mutationAddMessage({
          variables: {
            input: {
              text: message.trim(),
              userId: id,
            },
          },
        })
          .then(() => {
            setMessage('');
          })
          .catch(() => {
            globalSnackbarVar({
              open: true,
              message: 'An error occured',
              type: 'ERROR',
            });
          });
      } else if (!channelMessageLoading) {
        mutationAddChannelMessage({
          variables: {
            input: {
              text: message.trim(),
              channelId: id,
            },
          },
        })
          .then(() => {
            setMessage('');
          })
          .catch(() => {
            globalSnackbarVar({
              open: true,
              message: 'An error occured',
              type: 'ERROR',
            });
          });
      }
    }
  };

  const onKeyboardClick = (evt: KeyboardEvent<HTMLInputElement>) => {
    if ((evt.ctrlKey || evt.shiftKey) && evt.key === 'Enter') {
      evt.preventDefault();
      onSubmit();
    }
  };

  return (
    <div className={classes.containerMessageBox}>
      <CustomInput
        className={classes.inputMessage}
        placeholder="Message..."
        value={message}
        setValue={setMessage}
        multiline={true}
        variant="filled"
        maxRows={4}
        onKeyPress={onKeyboardClick}
        onSubmit={onSubmit}
      />
    </div>
  );
};

export default MessageBox;
