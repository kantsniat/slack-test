import { createTheme } from '@material-ui/core';
import { muiTheme } from 'storybook-addon-material-ui';
export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};

export const decorators = [
  muiTheme([
    createTheme({
      containerStyles: {
        flexColumn: {
          display: 'flex',
          flexDirection: 'column',
        },
        flexRow: {
          display: 'flex',
          flexDirection: 'row',
        },
        flexGrid: {
          display: 'grid',
        },
      },
      colors: {
        white: '#FFFFFF',
        black: '#000000',
        red: '#FF0000',
        gray: '#BDBDBD',
        pink: '#E91E63',
        grape: '#4a154b',
        codGray: '#1d1d1d',
        toryBlue: '#1264a3',
        pictonBlue: '#36c5f0',
        valentino: '#350d36',
      },
      textStyles: {
        maxWidth: '100%',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
      },
      containerStyles: {
        flexColumn: {
          display: 'flex',
          flexDirection: 'column',
        },
        flexRow: {
          display: 'flex',
          flexDirection: 'row',
        },
        flexGrid: {
          display: 'grid',
        },
      },
      colors: {
        white: '#FFFFFF',
        black: '#000000',
        red: '#FF0000',
        gray: '#BDBDBD',
        pink: '#E91E63',
        grape: '#4a154b',
        codGray: '#1d1d1d',
        toryBlue: '#1264a3',
        pictonBlue: '#36c5f0',
        valentino: '#350d36',
        jungleGreen: '#2BAC76',
        transparent: 'transparent',
      },
      fontFamily: {
        main: 'Slack-Circular-Pro,"Helvetica Neue",Helvetica,"Segoe UI",Tahoma,Arial,sans-serif;',
        secondary: 'Roboto, sans-serif',
      },
      boxShadow: {
        main: '0px 3px 20px rgba(0,0,0,0.16)',
        light: '0 1px 0 0 rgb(255 255 255 / 10%)',
      },
      palette: {
        primary: {
          main: '#4a154b',
        },
      },
      overrides: {
        MuiCircularProgress: {
          colorSecondary: {
            color: '#36c5f0',
          },
        },
      },
    }),
  ]),
];
